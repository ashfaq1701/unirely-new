<?php

namespace App\Listeners;

use Codemash\Socket\Events\MessageReceived;
use App\Repositories\MessageRepository;
use App\Repositories\ChannelRepository;
use App\Repositories\UserRepository;
use Event;
use Illuminate\Foundation\Bus\DispatchesJobs;
//use App\Jobs\EmailUndeliveredMessage;
use App\Jobs\EmailMessage;
use App\Models\Channel;
use Log;

class MessageReceivedListener
{
	use DispatchesJobs;
	
	protected $messageRepository;
	protected $channelRepository;
	protected $userRepository;
	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct(MessageRepository $messageRepository, ChannelRepository $channelRepository, UserRepository $userRepository)
	{
		$this->messageRepository = $messageRepository;
		$this->channelRepository = $channelRepository;
		$this->userRepository = $userRepository;
	}

	/**
	 * Handle the event.
	 *
	 * @param  MessageReceived  $event
	 * @return void
	 */
	public function handle(MessageReceived $event)
	{
		$user = $event->client->getUser();
		$message = $event->message;
		$command = $message->command;
		$data = $message->data;
		$message = $data->scalar;
		$admin = $this->userRepository->admin();
		
		if($command == 'typingStatus')
		{
			$statusObject = json_decode($message);
			$channelID = $statusObject->channel_id;
			$status = $statusObject->status;
			$channel = Channel::find($channelID);
			if($channel->blocked == 1)
			{
				return 0;
			}
			$otherUser = $channel->otherThanGivenUser($user);
			if(!(($otherUser->is_online == 0) || empty($otherUser->is_online)))
			{
				$clientId = $otherUser->connection_id;
				$client = $event->clients->get($clientId);
				if(!empty($client))
				{
					$client->send('typingStatus', json_encode([
							'status' => $status,
							'user' => $user,
							'channel' => $channel
					]));
				}
			}
		}
		
		if($command === 'textMessage')
		{
			$returnValue = $this->messageRepository->saveTextMessage($message, $user);
			if(empty($returnValue))
			{
				return 0;
			}
			$message = $returnValue['message'];
			$channel = $returnValue['channel'];
			$users = $channel->users;
			if(!$users->contains($admin))
			{
				if(!(($admin->is_online == 0) || empty($admin->is_online)))
				{
					$clientId = $admin->connection_id;
					$client = $event->clients->get($clientId);
					if(!empty($client))
					{
						$client->send('textMessage', json_encode($message));
					}
				}
			}
			foreach ($users as $currentUser)
			{
				if(!(($currentUser->is_online == 0) || empty($currentUser->is_online)))
				{
					$clientId = $currentUser->connection_id;
					$client = $event->clients->get($clientId);
					if(!empty($client))
					{
						$client->send('textMessage', json_encode($message));
					}
				}
				/*else
				{
					$this->dispatch(new EmailUndeliveredMessage($message));
					
				}*/
			}
			$otherUser = $channel->otherThanGivenUser($user);
			if(!(($otherUser->is_online == 0) || empty($otherUser->is_online)))
			{
				$clientId = $otherUser->connection_id;
				$client = $event->clients->get($clientId);
				if(!empty($client))
				{
					$client->send('typingStatus', json_encode([
							'status' => 0,
							'user' => $user,
							'channel' => $channel
					]));
				}
			}
			$this->dispatch(new EmailMessage($message, $user));
		}
		else if($command == 'fileMessage')
		{
			$returnValue = $this->messageRepository->saveFileMessage($message, $user);
			if(empty($returnValue))
			{
				return 0;
			}
			$message = $returnValue['message'];
			$channel = $returnValue['channel'];
			$users = $channel->users;
			if(!$users->contains($admin))
			{
				if(!(($admin->is_online == 0) || empty($admin->is_online)))
				{
					$clientId = $admin->connection_id;
					$client = $event->clients->get($clientId);
					if(!empty($client))
					{
						$client->send('fileMessage', json_encode($message));
					}
				}
			}
			foreach ($users as $currentUser)
			{
				if(!(($currentUser->is_online == 0) || empty($currentUser->is_online)))
				{
					$clientId = $currentUser->connection_id;
					$client = $event->clients->get($clientId);
					if(!empty($client))
					{
						$client->send('fileMessage', json_encode($message));
					}
				}
				/*else
				{
					$this->dispatch(new EmailUndeliveredMessage($message));
				}*/
			}
			$this->dispatch(new EmailMessage($message, $user));
		}
	}
}