<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Message;
use Illuminate\Contracts\Mail\Mailer;

class EmailUndeliveredMessage extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    
    protected $message;
    
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
    	$channel = $this->message->channel;
    	$users = $channel->users;
    	foreach ($users as $user)
    	{
    		if(($user->is_online == 0) || empty($user->is_online))
    		{
    			$mailer->send('emails.undelivered-mail', ['messageObj'=>$this->message], function($m) use($user)
    			{
    				$m->from(config('mail.username'), 'Unirely Email Notification');
    				$m->to($user->email, $user->name)->subject('Unirely : You received a new message');
    			});
    		}
    	}
    }
}