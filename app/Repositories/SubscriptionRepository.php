<?php

namespace App\Repositories;


use Carbon\Carbon;

class SubscriptionRepository
{
	public function payableSubscriptions($user, $from = null, $to = null)
	{
		$mentorshipsQuery = $user->mentorships()->where('valid_till', '<', Carbon::now());
		if(!empty($from))
		{
			$mentorshipsQuery = $mentorshipsQuery->where('valid_till', '>', $from);
		}
		if(!empty($to))
		{
			$mentorshipsQuery = $mentorshipsQuery->where('valid_till', '<', $to);
		}
		$mentorships = $mentorshipsQuery->get();  
		return $mentorships;
	}
	
	public function filteredWithdrawls($user, $from = null, $to = null)
	{
		$withdrawlsQuery = $user->withdrawls();
		if(!empty($from))
		{
			$withdrawlsQuery = $withdrawlsQuery->where('created_at', '>', $from);
		}
		if(!empty($to))
		{
			$withdrawlsQuery = $withdrawlsQuery->where('created_at', '<', $to);
		}
		$withdrawls = $withdrawlsQuery->get();
		return $withdrawls;
	}
}