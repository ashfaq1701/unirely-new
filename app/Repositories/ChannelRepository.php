<?php

namespace App\Repositories;

use Auth;
use App\Models\Channel;
use App\Models\Application;
use Carbon\Carbon;

class ChannelRepository
{
	protected $userRepository;
	
	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}
	
	public function getChannelMessages($channelId, $pageNumber)
	{
		$channel = Channel::find($channelId);
		$messages = $channel->messages()->with('user')->get();
		$messages = $messages->sortByDesc('created_at');
		$pagedMessages = $messages->slice($pageNumber*50, 50);
		$pagedMessages = $pagedMessages->all();
		$result = [
				'messages' => $pagedMessages,
				'more' => $this->haveMoreMessages($channelId, $pageNumber)
		];
		return $result;
	}
	
	public function haveMoreMessages($channelId, $pageNumber)
	{
		$channel = Channel::find($channelId);
		$messagesCount = $channel->messages()->count();
		$more = $messagesCount > (($pageNumber + 1) * 50) ? true : false;
		return $more;
	}
	
	public function markAllRead($channelId)
	{
		$user = Auth::user();
		$channel = Channel::find($channelId);
		$messages = $channel->unreadMessages();
		$channelMembers = $channel->users;
		if(!$channelMembers->contains($user))
		{
			return null;
		}
		foreach($messages as $message)
		{
			if($message->user_id != $user->id)
			{
				$message->read_at = Carbon::now();
				$message->save();
			}
		}
	}
	
	public function createOrReturnChannel($user1, $user2)
	{
		$channels = Channel::all();
		$userIds = [$user1->id, $user2->id];
		foreach ($channels as $channel)
		{
			$users = $channel->users;
			$currentIds = [];
			foreach ($users as $user)
			{
				$currentIds[] = $user->id;
			}
			if(array_count_values($userIds) == array_count_values($currentIds))
			{
				return $channel;
			}
		}
		$channel = new Channel();
		$channel->save();
		
		$channel->users()->attach($user1->id);
		$channel->users()->attach($user2->id);
		return $channel;
	}
	
	public function getAssignableApplications()
	{
		$applications = Application::has('subscription', '=', 0)->get();
		$resultApplications = array();
		foreach ($applications as $application)
		{
			$assignments = $application->assignments;
			$assignmentCount = $assignments->count();
			$calculatedCount = 0;
			foreach ($assignments as $assignment)
			{
				if($assignment->accepted == 1)
				{
					continue;
				}
				else if($assignment->accepted === 0)
				{
					$calculatedCount++;
				}
			}
			if($calculatedCount == $assignmentCount)
			{
				$resultApplications[] = $application;
			}
		}
		$applicationsCollect = collect($resultApplications);
		return $applicationsCollect;
	}
}