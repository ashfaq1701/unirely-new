<?php

namespace App\Repositories;


use Carbon\Carbon;
use User;
use App\Models\Role;
use GuzzleHttp\Client;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Message\Response;

class UserRepository
{
	public function admin()
	{
		$role = Role::where('name', 'admin')->first();
		$user = $role->users()->first();
		return $user;
	}
	
	public function adminChannel($user)
	{
		$channels = $user->channels;
		foreach ($channels as $channel)
		{
			if($channel->otherUser()->role->name == 'admin')
			{
				return $channel;
			}
		}
		return null;
	}
	
	public function userPendingApplications($user)
	{
		$applications = $user->applications;
		$pendingApplications = collect();
		foreach ($applications as $application)
		{
			if(empty($application->subscription))
			{
				$pendingApplications[] = $application;
			}
		}
		return $pendingApplications;
	}
	
	public function nonAdminChannels($user)
	{
		$channels = $user->channels;
		$nonAdminChannels = collect();
		foreach ($channels as $channel)
		{
			if($channel->otherThanGivenUser($user)->role->name != 'admin')
			{
				$nonAdminChannels->push($channel);
			}
		}
		return $nonAdminChannels;
	}
	
	public function nonAdminActiveChannels($user)
	{
		$activeChannels = collect();
		$channels = $this->nonAdminChannels($user);
		foreach ($channels as $channel)
		{
			if(!empty($channel->subscription))
			{
				if($channel->subscription->valid_till >= Carbon::now())
				{
					$activeChannels->push($channel);
				}
			}
		}
		return $activeChannels;
	}
	
	public function nonAdminExpiredChannels($user)
	{
		$expiredChannels = collect();
		$channels = $this->nonAdminChannels($user);
		foreach ($channels as $channel)
		{
			if(!empty($channel->subscription))
			{
				if($channel->subscription->valid_till < Carbon::now())
				{
					$expiredChannels->push($channel);
				}
			}
		}
		return $expiredChannels;
	}
}