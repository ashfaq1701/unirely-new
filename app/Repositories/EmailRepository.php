<?php

namespace App\Repositories;

use Illuminate\Contracts\Mail\Mailer;
use Mail;
use App\User;
use App\Repositories\UserRepository;

class EmailRepository
{	
	protected $admin;
	
	public function __construct()
	{
		$userRepository = new UserRepository();
		$this->admin = $userRepository->admin();
	}
	public function signupNotificationToAdmin($user)
	{
		if($user->role->name == 'student')
		{
			Mail::send('emails.student-signed-up-to-admin', ['user'=>$user], function($m) use($user)
			{
				$m->from(config('mail.username'), 'Unirely Email Notification');
				$m->to($this->admin->email, $user->name)->subject('Unirely : New Student Signup');
			});
		}
		else if($user->role->name == 'mentor')
		{
			Mail::send('emails.mentor-signed-up-to-admin', ['user'=>$user], function($m) use($user)
			{
				$m->from(config('mail.username'), 'Unirely Email Notification');
				$m->to($this->admin->email, $user->name)->subject('Unirely : New Mentor Signup');
			});
		}
	}
	
	public function mentorConnectionNotificationToStudent($assignment)
	{
		Mail::send('emails.assignment-accepted-by-mentor', ['assignment'=>$assignment], function($m) use($assignment)
		{
			$m->from(config('mail.username'), 'Unirely Email Notification');
			$m->to($assignment->application->user->email, $assignment->application->user->name)->subject('Unirely : Connected To Mentor');
		});
	}
	
	public function assignmentNotificationToMentor($assignment)
	{
		Mail::send('emails.mentor-assigned-to-student', ['assignment'=>$assignment], function($m) use($assignment)
		{
			$m->from(config('mail.username'), 'Unirely Email Notification');
			$m->to($assignment->user->email, $assignment->user->name)->subject('Unirely : New Assignment');
		});
	}
}