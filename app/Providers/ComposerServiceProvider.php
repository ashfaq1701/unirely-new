<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            ['chats.partials.admin-channel', 'chats.partials.admin-messaging-channel-list', 'chats.partials.mentor-messaging-channel-list', 'chats.partials.student-messaging-channel-list'], 
        	'App\Http\ViewComposers\ChannelComposer'
        );
        
        view()->composer(
        	'*',
        	'App\Http\ViewComposers\UniversityPackageComposer'
        );
        
        view()->composer(
        	'layouts.app',
        	'App\Http\ViewComposers\MenuComposer'
        );
        
        view()->composer(
        	['withdrawls.index', 'withdrawls.partials.mentorships'],
        	'App\Http\ViewComposers\WithdrawlComposer'
        );
        
        view()->composer(
        	'partials.sidebar',
        	'App\Http\ViewComposers\DashboardMenuComposer'
        );
        
        view()->composer(
        	'profile.profile',
        	'App\Http\ViewComposers\ProfileComposer'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}