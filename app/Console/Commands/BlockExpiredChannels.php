<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Subscription;
use Log;

class BlockExpiredChannels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'block:expired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Block expired channels periodically';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $subscriptions = Subscription::all();
        $blockedCount = 0;
        foreach ($subscriptions as $subscription)
        {
        	if(empty($subscription->blocked) || ($subscription->blocked == 0))
        	{
        		if($subscription->valid_till->isPast())
        		{
        			$subscription->blocked = 1;
        			$subscription->save();
        			
        			$channel = $subscription->channel;
        			$channel->blocked = 1;
        			$channel->save();
        			        			
        			$blockedCount++;
        		}
        	}
        }
        Log::info($blockedCount.' channels blocked');
    }
}
