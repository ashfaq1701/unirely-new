<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class MenuComposer
{
	/**
	 * Bind data to the view.
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$dropdownMenu = [
			'Reset Password' => [
				'roles' => ['admin', 'mentor', 'student'],
				'icon' => 'ico-key4',
				'url' => 'reset/password'
			],
			'My Earnings' => [
				'roles' => ['mentor'],
				'icon' => 'ico-money',
				'url' => 'withdrawls'
			],
			'My Profile' => [
				'roles' => ['mentor', 'admin', 'student'],
				'icon' => 'ico-profile',
				'url' => 'profile'
			]
		];
		$view->with('dropdownMenu', $dropdownMenu);
	}
}