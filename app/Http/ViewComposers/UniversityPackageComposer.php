<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Package;
use App\Models\University;

class UniversityPackageComposer
{
	/**
	 * Bind data to the view.
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$packages = Package::all();
		$universities = University::where('approved', 1)->get();
		
		$view->with('packages', $packages);
		$view->with('universities', $universities);
	}
}