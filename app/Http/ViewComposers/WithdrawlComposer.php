<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Auth;
use App\Repositories\SubscriptionRepository;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

class WithdrawlComposer
{
	protected $subscriptionRepository;
	protected $request;
	
	
	public function __construct(SubscriptionRepository $subscriptionRepository, Request $request)
	{
		$this->subscriptionRepository = $subscriptionRepository;
		$this->request = $request;
	}
	/**
	 * Bind data to the view.
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$data = $view->getData();
		
		if(!isset($data['userId']))
		{
			$user = Auth::user();
		}
		else
		{
			$user = User::find($data['userId']);
		}
		
		$mentorshipFrom = null;
		$mentorshipTo = null;
		$withdrawlFrom = null;
		$withdrawlTo = null;
		
		if($this->request->has('mentorship-from'))
		{
			$mentorshipFrom = Carbon::createFromFormat('m/d/Y', $this->request->input('mentorship-from'));
		}
		if($this->request->has('mentorship-to'))
		{
			$mentorshipTo = Carbon::createFromFormat('m/d/Y', $this->request->input('mentorship-to'));	
		}
		if($this->request->has('withdrawl-from'))
		{
			$withdrawlFrom = Carbon::createFromFormat('m/d/Y', $this->request->input('withdrawl-from'));
		}
		if($this->request->has('withdrawl-to'))
		{
			$withdrawlTo = Carbon::createFromFormat('m/d/Y', $this->request->input('withdrawl-to'));
		}
		
		$mentorships = $this->subscriptionRepository->payableSubscriptions($user, $mentorshipFrom, $mentorshipTo);
		$withdrawls = $this->subscriptionRepository->filteredWithdrawls($user, $withdrawlFrom, $withdrawlTo);
		
		$totalWithdrawl = 0;
		foreach ($withdrawls as $withdrawl)
		{
			$totalWithdrawl = $totalWithdrawl + $withdrawl->amount;
		}
		
		$totalPayable = 0;
		foreach ($mentorships as $mentorship)
		{
			$totalPayable = $totalPayable + ($mentorship->application->package->payment * $user->payment_factor);
		}
		
		$balance = $totalPayable - $totalWithdrawl;
		$view->with('user', $user);
		$view->with('mentorships', $mentorships);
		$view->with('withdrawls', $withdrawls);
		$view->with('totalWithdrawl', $totalWithdrawl);
		$view->with('totalPayable', $totalPayable);
		$view->with('balance', $balance);
	}
}