<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\University;

class ProfileComposer
{
	/**
	 * Bind data to the view.
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$universities = University::all();
		$view->with('universities', $universities);
	}
}
	