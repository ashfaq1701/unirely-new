<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Repositories\ChannelRepository;
use App\Models\Application;
use Auth;
use Route;
use App\Repositories\UserRepository;

class ChannelComposer
{
	protected $userRepository;
	protected $channelRepository;
	
	public function __construct(UserRepository $userRepository, ChannelRepository $channelRepository)
	{
		$this->userRepository = $userRepository;
		$this->channelRepository = $channelRepository;
	}
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
    	$route = Route::current();
    	$view->with('route', $route);
    	$user = Auth::user();
    	$channels = $user->channels;
    	$view->with('channels', $channels);
    	
    	if($user->role->name != 'admin')
    	{
    		$adminChannel = $this->userRepository->adminChannel($user);
    		$activeChannels = $this->userRepository->nonAdminActiveChannels($user);
    		$expiredChannels = $this->userRepository->nonAdminExpiredChannels($user);
    		$view->with('adminChannel', $adminChannel);
    		$view->with('activeChannels', $activeChannels);
    		$view->with('expiredChannels', $expiredChannels);
    	}
    	
    	else
    	{
    		$applications = $this->channelRepository->getAssignableApplications();
    		$activeChannels = $user->channels;
    		$view->with('applications', $applications);
    		$view->with('activeChannels', $activeChannels);
    	}
    	
    	if($user->role->name == 'student')
    	{
    		$applications = $user->applications()->has('subscription', '=', 0)->get();
    		$view->with('applications', $applications);
    	}
    	
    	if($user->role->name == 'mentor')
    	{
    		$assignments = $user->assignments()->whereNull('accepted')->get();
    		$view->with('assignments', $assignments);
    	}
    }
}