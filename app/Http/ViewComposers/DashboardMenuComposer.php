<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

class DashboardMenuComposer
{
	/**
	 * Bind data to the view.
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$menu = [
			'Mentors' => [
				'roles' => ['admin'],
				'icon' => 'ico-user',
				'items' => [
					'Pending Mentors' => [
						'roles' => ['admin'],
						'url' => '/mentors/pending'
					],
					'All Mentors' => [
						'roles' => ['admin'],
						'url' => '/mentors'
					]
				]
			],
			'Students' => [
				'roles' => ['admin'],
				'icon' => 'ico-user',
				'items' => [
					'All Students' => [
						'roles' => ['admin'],
						'url' => '/students'
					]
				]
			],
			'Universities' => [
				'roles' => ['admin'],
				'icon' => 'ico-book',
				'items' => [
					'Universities' => [
						'roles' => ['admin'],
						'url' => '/universities'
					]
				]
			]
		];
		$view->with('menu', $menu);
	}
}