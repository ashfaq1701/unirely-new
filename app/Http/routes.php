<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Authentication Routes...
Route::get('login', 'Auth\AuthController@showLoginForm');
Route::post('login', 'Auth\AuthController@login')->middleware('user-id.write');
Route::get('logout', 'Auth\AuthController@logout')->middleware('user-id.delete');
	
// Registration Routes...
Route::get('register', 'Auth\AuthController@showRegistrationForm');
Route::post('register', 'Auth\AuthController@register')->middleware('user-id.write');

// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');

Route::get('universities/find', 'UniversityController@findUniversities');
Route::post('university/request', 'UniversityController@requestUniversity');

Route::get('user/activation/{token}', 'Auth\AuthController@activateUser')->name('user.activate');


Route::group(['middleware' => ['auth']], function () 
{
	Route::get('/home', 'HomeController@index');
	
	Route::get('/channel/messages/{channelId}/{pageNumber?}', 'ChannelController@messages');
	Route::get('/channel/markread/{channelId}', 'ChannelController@markMessagesRead');
	Route::post('/uploads/file', 'UploadController@fileUpload')->name('file.upload');

	//Universities Route

	Route::get('universities', 'UniversityController@universities')->name('university.list');
	Route::get('university/approve/{universityId}', 'UniversityController@approveUniversity');
	Route::get('university/bann/{universityId}', 'UniversityController@bannUniversity');

	//User Routes
	Route::get('mentors', 'UserController@allMentors');
	Route::get('mentors/pending', 'UserController@pendingMentors');
	Route::get('students', 'UserController@allStudents');
	Route::get('mentor/approve/{mentorId}', 'UserController@approveMentor');
	Route::get('mentor/bann/{mentorId}', 'UserController@bannMentor');
	Route::get('user/chat-list/{userId}', 'UserController@chatList');

	Route::post('application/create', 'ApplicationController@createApplications');
	Route::get('application/view/{applicationId}', 'ApplicationController@viewApplicationStatus');

	Route::get('checkout', 'PaymentController@viewCheckoutForm');
	Route::post('checkout', 'PaymentController@handleCheckout');

	Route::get('application/assign/{applicationId}', 'ApplicationController@getAssignApplication');
	Route::get('assign/application/{applicationId}/{mentorId}', 'ApplicationController@assignToMentor');

	Route::get('assignment/view/{assignmentId}', 'AssignmentController@viewAssignment');

	Route::get('assignment/accept/{assignmentId}', 'AssignmentController@acceptAssignment');
	Route::get('assignment/reject/{assignmentId}', 'AssignmentController@rejectAssignment');

	Route::get('chat/top/{channelId}', 'HomeController@chatTop');

	Route::get('renew/{channelId}', 'ChannelController@getRenewChannel');
	Route::post('renew/{channelId}', 'ChannelController@postRenewChannel');

	Route::get('reset/password', 'PasswordController@resetPasswordForm');
	Route::post('reset/password', 'PasswordController@postResetPassword');

	Route::get('withdrawls/{userId?}', 'WithdrawlController@withdrawlsPage')->where('userId', '[0-9]+');
	Route::post('withdrawls/{userId?}', 'WithdrawlController@withdrawlsPage')->where('userId', '[0-9]+');

	Route::post('withdrawls/request', 'WithdrawlController@requestWithdrawl');

	Route::get('dashboard', 'DashboardController@viewDashboard');

	Route::get('profile/{userId?}', 'ProfileController@viewProfile');
	Route::post('profile/save/{userId?}', 'ProfileController@saveProfile')->where('userId', '[0-9]+');
	Route::post('profile/image', 'ProfileController@saveProfileImage');

	Route::get('courses/find', 'CourseController@findCourses');
	Route::post('courses/create', 'CourseController@createCourse');

	Route::get('images/{filename}', 'FileController@imageDownload');
	Route::get('files/{filename}', 'FileController@fileDownload');

	Route::post('uploads/image', 'UploadController@imageUpload')->name('upload.image');
	Route::post('/uploads/file', 'UploadController@fileUpload')->name('file.upload');

	Route::get('channel/isactive/{channelId}', 'ChannelController@isActiveChannel');

	Route::post('users/peer/save', 'UserController@savePeerId');
	Route::get('user/peer', 'UserController@getUserByPeerId');
});