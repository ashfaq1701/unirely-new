<?php

namespace App\Http\Middleware;

use Closure;

class RoleAccessMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
    	if($request->user()->role->name == $role)
    	{
    		return $next($request);
    	}
    	abort(403, 'You are not allowed to access this page');
    }
}
