<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class WriteUserIdCookie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	$response = $next($request);
    	
    	if(!empty(Auth::user()))
    	{
    		return $response->cookie('user-id', Auth::user()->email, 15*24*60, null, null, false, false);
    	}
    	else
    	{
    		return $response;
    	}
    }
}
