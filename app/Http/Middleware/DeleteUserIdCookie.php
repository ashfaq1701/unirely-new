<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;

class DeleteUserIdCookie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $cookie1 = Cookie::forget('user-id');
        return $response->withCookie($cookie1);
    }
}
