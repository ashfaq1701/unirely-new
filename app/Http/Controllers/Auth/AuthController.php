<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Auth;
use App\Models\Role;
use App\Models\University;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $redirectAfterLogout = '/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
        	'phone' => 'required',
            'password' => 'required|min:6|confirmed',
        	'role' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
    	$post = [
    		'name' => $data['name'],
    		'email' => $data['email'],
    		'password' => bcrypt($data['password']),
    		'phone' => $data['phone'],
    		'role_id' => $data['role'],
    		'profile_image' => config('constants.dummy_profile_image')
    	];
    	if(!empty($data['university']) && (Role::find($data['role'])->name == 'mentor'))
    	{
    		$post['university_id'] = $data['university'];
    	}
        $user = User::create($post);
        return $user;
    }
    
    public function showRegistrationForm()
    {
    	if (property_exists($this, 'registerView')) {
    		return view($this->registerView);
    	}
    	$roles = Role::where('name', '!=', 'admin')->get();
    	$universities = University::all();
    	return view('auth.register', ['roles'=>$roles, 'universities'=>$universities]);
    }
}
