<?php

namespace App\Http\Controllers;

use App\Repositories\UploadRepository;

class UploadController extends Controller
{
	protected $uploadRepository;
	
	public function __construct(UploadRepository $uploadRepository)
	{
		$this->uploadRepository = $uploadRepository;
	}
	
	public function imageUpload()
	{
		$uploadedFiles = $this->uploadRepository->imageUpload();
		
		return response()->json(["status"=>"ok", "files"=>$uploadedFiles]);
	}
	
	public function fileUpload()
	{
		$uploadedFiles = $this->uploadRepository->fileUpload();
	
		return response()->json(["status"=>"ok", "files"=>$uploadedFiles]);
	}
}