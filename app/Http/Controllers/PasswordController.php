<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Hash;

class PasswordController extends Controller
{
	public function resetPasswordForm()
	{
		return view('auth.passwords.reset-auth');
	}
	
	public function postResetPassword(Request $request)
	{
		$this->validate($request, [
			'password' => 'required',
			'new_password' => 'required|min:6|max:20|confirmed',
		]);
		$user = Auth::user();
		$password = $request->get('password');
		$newPassword = $request->get('new_password');
		if(!Hash::check($password, $user->password))
		{
			return redirect('/reset/password')->with('warning', 'Password is not correct');
		}
		$user->password = bcrypt($newPassword);
		$user->save();
		return redirect('/reset/password')->with('status', 'Password changed successfully');
	}
}