<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\User;
use Auth;

class UserController extends Controller
{
	protected $userRepository;
	
	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}
	
	public function allMentors()
	{
		$mentors = User::allMentors()->get();
		return view('admin.all-mentors', ['mentors' => $mentors]);
	}
	
	public function pendingMentors()
	{
		$mentors = User::inactiveMentors()->get();
		return view('admin.pending-mentors', ['mentors' => $mentors]);
	}
	
	public function allStudents()
	{
		$students = User::allStudents()->get();
		return view('admin.all-students', ['students' => $students]);
	}
	
	public function approveMentor($mentorId)
	{
		$user = User::find($mentorId);
		$user->active = 1;
		$user->save();
		return response()->json(['status'=>true]);
	}
	
	public function bannMentor($mentorId)
	{
		$user = User::find($mentorId);
		$user->active = 0;
		$user->save();
		return response()->json(['status'=>true]);
	}
	
	public function chatList($userId)
	{
		$user = User::find($userId);
		$channels = $this->userRepository->nonAdminChannels($user);
		return view('admin.chat-list', ['channels' => $channels, 'user'=> $user]);
	}
	
	public function savePeerId(Request $request)
	{
		$peerId = $request->input('peer_id');
		$user = Auth::user();
		$user->peer_id = $peerId;
		$user->save();
		$cookie = cookie('peer_id', $peerId, 15*24*60, null, null, false, false);
		return response()->json(['status'=>true, 'message'=>'peer id saved'])->withCookie($cookie);
	}
	
	public function getUserByPeerId(Request $request)
	{
		$peerId = $request->input('peer_id');
		$user = User::where('peer_id', $peerId)->get()->first();
		return response()->json(['user' => $user]);
	}
}