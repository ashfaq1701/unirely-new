<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Channel;
use App\Models\Payment;
use App\Models\Package;
use App\Models\Application;
use App\Models\Assignment;
use Exception;
use Carbon\Carbon;
use App\Repositories\ChannelRepository;
use App\Repositories\EmailRepository;

class ChannelController extends Controller
{
	protected $channelRepository;
	protected $emailRepository;
	
	public function __construct(ChannelRepository $channelRepository, EmailRepository $emailRepository)
	{
		$this->channelRepository = $channelRepository;
		$this->emailRepository = $emailRepository;
	}
	public function getRenewChannel($channelId)
	{
		$channel = Channel::find($channelId);
		$packages = Package::all();
		return view('payments.renew', ['channel'=>$channel, 'packages'=>$packages]);
	}
	
	public function postRenewChannel(Request $request, $channelId)
	{
		$channel = Channel::find($channelId);
		$mentor = $request->input('mentor');
		$user = Auth::user();
		if($request->has('cardBrand') && $request->has('stripeToken') && $request->has('last4'))
		{
			$cardBrand = $request->input('cardBrand');
			$stripeToken = $request->input('stripeToken');
			$lastFour = $request->input('last4');
			$user->createAsStripeCustomer($stripeToken);
			$user->card_brand = $cardBrand;
			$user->card_last_four = $lastFour;
			$user->save();
		}
		try
		{
			$package = Package::find($request->input('package'));
			$response = $user->invoiceFor('Renewal Fees', ($package->payment)*100);
			$invoiceId = $response->id;
			$chargeId = $response->charge;
				
			$payment = new Payment();
			$payment->amount = $package->payment;
			$payment->invoice_id = $invoiceId;
			$payment->charge_id = $chargeId;
			$payment->save();
			
			$application = new Application();
			$application->package_id = $package->id;
			$application->university_id = $channel->subscription->application->university->id;
			$application->user_id = $user->id;
			$application->payment_id = $payment->id;
			$application->save();
			
			$detail = [
				'university' => $channel->subscription->application->university->name,
				'package' => $package->name
			];
			
			if($mentor == 'same')
			{
				$assignment = new Assignment();
				$assignment->application_id = $application->id;
				$assignment->user_id = $channel->subscription->mentor->id;
				$assignment->save();
				$this->emailRepository->assignmentNotificationToMentor($assignment);
				$detail['mentor'] = $channel->subscription->mentor->name;
			}
			return response()
			->view('payments.renew-confirm', ['detail'=>$detail]);
		}
		catch(Exception $e)
		{
			return response()
			->view('payments.failed-renew', ['e'=>$e]);
		}
	}
	
	public function messages($channelId, $pageNumber = 0)
	{
		$result = $this->channelRepository->getChannelMessages($channelId, $pageNumber);
		return response()->json($result);
	}
	
	public function markMessagesRead($channelId)
	{
		$this->channelRepository->markAllRead($channelId);
	}
	
	public function isActiveChannel($channelId)
	{
		$channel = Channel::find($channelId);
		$status = true;
		$subscription = $channel->subscription;
		if(!empty($subscription))
		{
			$validTill = $subscription->valid_till;
			if(Carbon::now() > $subscription->valid_till)
			{
				$status = false;
			}
		}
		return response()->json(['status'=>$status]);
	}
}