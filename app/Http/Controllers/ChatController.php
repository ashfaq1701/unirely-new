<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ChannelRepository;
class ChatController extends Controller
{
	protected $channelRepository;
	
	public function __construct(ChannelRepository $channelRepository)
	{
		$this->channelRepository = $channelRepository;
	}
}