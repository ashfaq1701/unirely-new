<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\SubscriptionRepository;
use App\Repositories\UserRepository;
use Auth;
use Auth\User;
use App\Models\Withdrawl;
use Mail;

class WithdrawlController extends Controller
{
	protected $subscriptionRepository;
	
	public function __construct(SubscriptionRepository $subscriptionRepository, UserRepository $userRepository)
	{
		$this->subscriptionRepository = $subscriptionRepository;
		$this->userRepository = $userRepository;
	}
	
	public function withdrawlsPage($userId = null)
	{
		return view('withdrawls.index', ['userId'=>$userId]);
	}
	
	public function requestWithdrawl(Request $request)
	{
		$amount = $request->input('amount');
		$admin = $this->userRepository->admin();
		$user = Auth::user();
		$mentorships = $this->subscriptionRepository->payableSubscriptions($user);
		$withdrawls = $user->withdrawls;
		
		$totalWithdrawl = 0;
		foreach ($withdrawls as $withdrawl)
		{
			$totalWithdrawl = $totalWithdrawl + $withdrawl->amount;
		}
		
		$totalPayable = 0;
		foreach ($mentorships as $mentorship)
		{
			$totalPayable = $totalPayable + ($mentorship->application->package->payment * $user->payment_factor);
		}
		
		$balance = $totalPayable - $totalWithdrawl;
		if($amount > $balance)
		{
			return redirect('/withdrawls')->with('warning', 'You can request maximum withdrawl of '.$balance);
		}
		$withdrawl = new Withdrawl();
		$withdrawl->amount = $amount;
		$withdrawl->user_id = $user->id;
		$withdrawl->save();
		Mail::send('withdrawls.emails.admin-withdrawl-notification', ['user' => $user, 'amount' => $amount], function ($m) use ($admin) {
			$m->to($admin->email, $admin->name)->subject('Withdrawl Notification');
		});
		return redirect('/withdrawls')->with('status', 'Your withdrawl of USD '.$amount.' was successful');
	}
}