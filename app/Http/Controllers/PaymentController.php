<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Application;
use App\Models\Payment;
use Cookie;
use Exception;

class PaymentController extends Controller
{
	public function viewCheckoutForm(Request $request)
	{
		$details = json_decode($request->cookie('billing_details'), true);
		$totalPrice = 0;
		foreach ($details as $detail)
		{
			$totalPrice += $detail['price'];
		}
		return view('payments.checkout', ['totalPrice'=>$totalPrice, 'details'=>$details]);
	}
	
	public function handleCheckout(Request $request)
	{
		$user = Auth::user();
		if($request->has('cardBrand') && $request->has('stripeToken') && $request->has('last4'))
		{
			$cardBrand = $request->input('cardBrand');
			$stripeToken = $request->input('stripeToken');
			$lastFour = $request->input('last4');
			$user->createAsStripeCustomer($stripeToken);
			$user->card_brand = $cardBrand;
			$user->card_last_four = $lastFour;
			$user->save();
		}
		
		$details = json_decode($request->cookie('billing_details'), true);
		$totalPrice = 0;
		foreach ($details as $detail)
		{
			$totalPrice += $detail['price'];
		}
		try 
		{
			$response = $user->invoiceFor('Subscription Fees', $totalPrice*100);
			$invoiceId = $response->id;
			$chargeId = $response->charge;
			
			$payment = new Payment();
			$payment->amount = $totalPrice;
			$payment->invoice_id = $invoiceId;
			$payment->charge_id = $chargeId;
			$payment->save();
			
			foreach ($details as $detail)
			{		
				$application = new Application();
				$application->package_id = $detail['package_id'];
				$application->university_id = $detail['university_id'];
				$application->user_id = $user->id;
				$application->payment_id = $payment->id;
				$application->save();
			}
			return response()
			->view('payments.confirmation', ['details'=>$details])
			->cookie('billing_details','[]');
		}
		catch (Exception $e)
		{
			return response()
				->view('payments.failed', ['e'=>$e]);
		}
		
	}
}