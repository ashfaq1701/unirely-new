<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\University;

class UniversityController extends Controller
{
	public function findUniversities(Request $request)
	{
		$universityName = $request->input('name');
		$universities = University::where('name', 'LIKE', '%'.$universityName.'%')->get();
		return response()->json($universities);
	}
	
    public function requestUniversity(Request $request)
    {
    	$name = $request->input('name');
    	$university = new University();
    	$university->name = $name;
    	$university->save();
    	return response()->json($university);
    }
    
    public function universities(Request $request)
    {
    	if($request->has('active'))
    	{
    		$active = $request->input('active');
    		if($active == 0)
    		{
    			$universities = University::inactiveUniversities()->get();
    		}
    		else
    		{
    			$universities = University::activeUniversities()->get();
    		}
    	}
    	else
    	{
    		$universities = University::all();
    	}
    	return view('admin.universities', ['universitiesTable'=>$universities]);
    }
    
    public function createUniversity()
    {
    	return view('universities.create');
    }
    
    public function editUniversity($universityId)
    {
    	$university = University::find($universityId);
    	return view('universities.edit', ['university'=>$university]);
    }
    
    public function updateUniversity(Request $request, $universityId)
    {
    	$this->validate($request, [
    		'name' => 'required|max:255'
    	]);
    	$name = $request->input('name');
    	$university = University::find($universityId);
    	$university->name = $name;
    	$university->save();
    	return redirect()->route('university.list');
    }
    
    public function storeUniversity(Request $request)
    {
    	$this->validate($request, [
    		'name' => 'required|max:255'
    	]);
    	$name = $request->input('name');
    	$university = new University();
    	$university->name = $name;
    	$university->save();
    	return redirect()->route('university.list');
    }
    
    public function approveUniversity($universityId)
    {
    	$university = University::find($universityId);
    	$university->approved = 1;
    	$university->save();
    	return response()->json(['status'=>true]);
    }
    
    public function bannUniversity($universityId)
    {
    	$university = University::find($universityId);
    	$university->approved = 0;
    	$university->save();
    	return response()->json(['status'=>true]);
    }
}