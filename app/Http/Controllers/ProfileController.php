<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\UserMeta;
use Auth;
use App\Repositories\UserRepository;

class ProfileController extends Controller
{
	protected $userRepository;
	
	public function __construct(UserRepository $userRepository)
	{
		$this->userRepository = $userRepository;
	}
	
	public function viewProfile($userId = null)
	{
		if(empty($userId))
		{
			$user = Auth::user();
		}
		else
		{
			$user = User::find($userId);
		}
		return view('profile.profile', ['user'=>$user]);
	}
	
	public function saveProfile(Request $request, $userId = null)
	{
		if(empty($userId))
		{
			$user = Auth::user();
		}
		else
		{
			$user = User::find($userId);
		}
		$this->validate($request, [
			'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users,email,'.$user->id,
			'phone' => 'required|max:20'
		]);
		
		$user->name = $request->input('name');
		$user->email = $request->input('email');
		$user->phone = $request->input('phone');
		
		if(count($user->userMeta))
		{
			$userMeta = $user->userMeta;
		}
		else
		{
			$userMeta = new UserMeta();
			$userMeta->user_id = $user->id;
			$userMeta->save();
		}
		
		if($request->has('city'))
		{
			$userMeta->city = $request->input('city');
		}
		
		if($request->has('university'))
		{
			$user->university_id = $request->input('university');
		}
		
		if($request->has('percentage'))
		{
			$userMeta->percentage = $request->input('percentage');
		}
		
		if($request->has('courses-interested'))
		{
			$courses = explode(',', $request->input('courses-interested'));
			$user->courses()->sync($courses);
		}
		
		$user->save();
		$userMeta->save();
		return redirect('/profile')->with('status', 'Profile saved successfully');
	}
	
	public function saveProfileImage(Request $request)
	{
		$user = User::where('email', $request->input('userId'))->first();
		$user->profile_image = $request->input('image');
		$user->save();
		return response()->json(['status'=>'ok']);
	}
}