<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;

class CourseController extends Controller
{
	public function findCourses(Request $request)
	{
		$courseName = $request->input('name');
		$courses = Course::where('name', 'LIKE', '%'.$courseName.'%')->get();
		return response()->json($courses);
	}
	
	public function createCourse(Request $request)
	{
		$courseName = $request->input('name');
		$course = new Course();
		$course->name = $courseName;
		$course->save();
		return response()->json($course);
	}
}