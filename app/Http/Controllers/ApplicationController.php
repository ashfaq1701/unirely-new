<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Package;
use App\Models\Application;
use App\Models\University;
use App\User;
use App\Models\Assignment;
use App\Repositories\EmailRepository;

class ApplicationController extends Controller
{
	public $emailRepository;
	
	public function __construct(EmailRepository $emailRepository)
	{
		$this->emailRepository = $emailRepository;
	}
	
    public function createApplications(Request $request)
    {
    	$packages = Package::all();
    	$packageArray = array();
    	foreach($packages as $package)
    	{
    		$packageArray[$package->payment] = $package->id;
    	}
    	$results = array();
    	$applications = json_decode($request->input('applications'), true);
    	$totalPrice = 0;
    	foreach ($applications as $applicationStr=>$price)
    	{
    		$totalPrice += $price;
    		
    		$applicationParts = explode('-', $applicationStr);
    		$universityId = $applicationParts[count($applicationParts)-1];
    		$packageId = $packageArray[$price];
    		
    		$results[] = [
    			'university_id' => $universityId,
    			'university_name' => University::find($universityId)->name,
    			'package_id' => $packageId,
    			'package_name' => Package::find($packageId)->name,
    			'price' => $price
    		];
    	}
    	return redirect('checkout')->cookie('billing_details', json_encode($results));
    }
    
    public function getAssignApplication($applicationId)
    {
    	$application = Application::find($applicationId);
    	$mentors = University::find($application->university->id)->mentors;
    	return view('chats.partials.content-area-admin-approval', ['application'=>$application, 'mentors'=>$mentors]);
    }
    
    public function assignToMentor($applicationId, $mentorId)
    {
    	$application = Application::find($applicationId);
    	$user = User::find($mentorId);
    	$assignment = new Assignment();
    	$assignment->application_id = $application->id;
    	$assignment->user_id = $user->id;
    	$assignment->save();
    	$this->emailRepository->assignmentNotificationToMentor($assignment);
    	return response()->json(['status' => true, 'message' => 'Application assigned to selected mentor']);
    }
    
    public function viewApplicationStatus($applicationId)
    {
    	$application = Application::find($applicationId);
    	return view('chats.partials.content-area-pending-application', ['application'=>$application]);
    }
}
