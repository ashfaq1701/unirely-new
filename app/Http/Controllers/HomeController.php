<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Package;
use App\Models\Application;
use App\Models\Channel;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	return view('chats.chat');
    }
    
    public function chatTop($channelId)
    {
    	$channel = Channel::find($channelId);
    	$user = Auth::user();
    	$otherUser = $channel->otherUser();
    	if($otherUser->role->name == 'admin')
    	{
    		return view('chats.partials.admin-channel-top', ['channel' => $channel]);
    	}
    	if($user->role->name == 'admin')
    	{
    		return view('chats.partials.chat-top-admin', ['user' => $otherUser, 'channel'=>$channel]);
    	}
    	else
    	{
    		$now = Carbon::now();
    		$validTill = $channel->subscription->valid_till;
    		$difference = $now->diffInDays($validTill, false);
    		if($user->role->name == 'mentor')
    		{
    			return view('chats.partials.chat-top-mentor', ['user' => $otherUser, 'plan' => $channel->subscription->application->package->name, 'difference'=>$difference, 'channel'=>$channel]);
    		}
    		else
    		{	
    			return view('chats.partials.chat-top-student', ['user' => $otherUser, 'plan' => $channel->subscription->application->package->name, 'difference'=>$difference, 'channel'=>$channel]);
    		}
    	}
    }
}
