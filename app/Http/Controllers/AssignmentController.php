<?php 

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Models\Assignment;
use App\Models\Subscription;
use Carbon\Carbon;
use App\Repositories\ChannelRepository;
use App\Repositories\EmailRepository;

class AssignmentController extends Controller
{
	protected $channel_repository;
	protected $emailRepository;
	
	public function __construct(ChannelRepository $channel_repository, EmailRepository $emailRepository)
	{
		$this->channel_repository = $channel_repository;
		$this->emailRepository = $emailRepository;
	}
	
	public function viewAssignment($assignmentId)
	{
		$assignment = Assignment::find($assignmentId);
		return view('chats.partials.content-area-view-assignment', ['assignment'=>$assignment]);
	}
	
	public function acceptAssignment($assignmentId)
	{
		$assignment = Assignment::find($assignmentId);
		$subscription = new Subscription();
		$subscription->mentor_id = Auth::user()->id;
		$subscription->application_id = $assignment->application->id;
		$subscription->user_id = $assignment->application->user->id;
		$subscription->valid_till = Carbon::now()->addDays($assignment->application->package->validity);
		$subscription->save();
		$user1 = $assignment->application->user;
		$user2 = Auth::user();
		$channel = $this->channel_repository->createOrReturnChannel($user1, $user2);
		$channel->subscription_id = $subscription->id;
		$channel->blocked = 0;
		$channel->save();
		$assignment->accepted = 1;
		$assignment->save();
		$this->emailRepository->mentorConnectionNotificationToStudent($assignment);
		return view('chats.partials.mentor-new-chat-section-partial', compact('channel'));
	}
	
	public function rejectAssignment($assignmentId)
	{
		$assignment = Assignment::find($assignmentId);
		$assignment->accepted = 0;
		$assignment->save();
		return response()->json(['status' => true, 'message' => 'Assignment has been rejected']);
	}
}

?>