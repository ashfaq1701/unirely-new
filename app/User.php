<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use GuzzleHttp\Client;
use GuzzleHttp\Message\Request;
use GuzzleHttp\Message\Response;
use GuzzleHttp\Exception\ClientException;
use App\Models\Role;
use App\Models\Channel;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use App\Repositories\UserRepository;
use App\Repositories\EmailRepository;

class User extends Authenticatable
{
	use Billable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email', 'role_id', 'profile_image', 'university_id', 'phone', 'payment_factor'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $dates = [
    	'created_at',
    	'updated_at',
    	'deleted_at',
    	'last_seen_at'
    ];
    
    public function channels()
    {
    	return $this->belongsToMany('App\Models\Channel');
    }
    
    public function messages()
    {
    	return $this->hasMany('App\Models\Message');
    }
    
    public function scopeAllMentors($query)
    {
    	return $query->whereHas('role', function ($query) {
    		$query->where('name', 'like', 'mentor');
    	});
    }
    
    public function scopeAllStudents($query)
    {
    	return $query->whereHas('role', function ($query) {
    		$query->where('name', 'like', 'student');
    	});
    }
    
    public function scopeActiveMentors($query)
    {
    	return $query->whereHas('role', function ($query) {
    		$query->where('name', 'like', 'mentor');
		})->where('active', 1);
    }
    
    public function scopeInactiveMentors($query)
    {
    	return $query->whereHas('role', function ($query) {
    		$query->where('name', 'like', 'mentor');
		})->whereNull('active')->orWhere('active', 0);
    }
    
    public function role()
    {
    	return $this->belongsTo('App\Models\Role');
    }
    
    public function university()
    {
    	return $this->belongsTo('App\Models\University');
    }
    
    public function applications()
    {
    	return $this->hasMany('App\Models\Application');
    }
    
    public function mentorships()
    {
    	return $this->hasMany('App\Models\Subscription', 'mentor_id');
    }
    
    public function subscriptions()
    {
    	return $this->hasMany('App\Models\Subscription', 'user_id');
    }
    
    public function userMeta()
    {
    	return $this->hasOne('App\Models\UserMeta');
    }
    
    public function assignments()
    {
    	return $this->hasMany('App\Models\Assignment');
    }
    
    public function withdrawls()
    {
    	return $this->hasMany('App\Models\Withdrawl', 'user_id');
    }
    
    public function courses()
    {
    	return $this->belongsToMany('App\Models\Course');
    }
    
    public static function boot() {
    	static::created(function(User $user) {
    		$user_repository = new UserRepository();
    		$admin = $user_repository->admin();
    		$channel = new Channel();
    		$channel->save();
    		
    		$channel->users()->attach($admin->id);
    		$channel->users()->attach($user->id);
    		
    		$emailRepository = new EmailRepository();
    		$emailRepository->signupNotificationToAdmin($user);
    	});
    	parent::boot();
    }
}
