<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    public function mentors()
    {
    	return $this->hasMany('App\User');
    }
    
    public function applications()
    {
    	return $this->hasMany('App\Models\Application');
    }
    
    public function scopeActiveUniversities($query)
    {
    	return $query->where('approved', 1);
    }
    
    public function scopeInactiveUniversities($query)
    {
    	return $query->whereNull('approved')->orWhere('approved', 0);
    }
}
