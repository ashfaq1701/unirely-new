<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{   
	protected $dates = ['created_at', 'updated_at', 'valid_till'];
    public function application()
    {
    	return $this->belongsTo('App\Models\Application');
    }
    
    public function mentor()
    {
    	return $this->belongsTo('App\User', 'mentor_id');
    }
    
    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
    
    public function channel()
    {
    	return $this->hasOne('App\Models\Channel');
    }
}
