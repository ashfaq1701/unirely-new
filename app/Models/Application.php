<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    public function package()
    {
    	return $this->belongsTo('App\Models\Package');
    }
    
    public function payment()
    {
    	return $this->belongsTo('App\Models\Payment');
    }
    
    public function university()
    {
    	return $this->belongsTo('App\Models\University');
    }
    
    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    
    public function subscription()
    {
    	return $this->hasOne('App\Models\Subscription');
    }
    
    public function assignments()
    {
    	return $this->hasMany('App\Models\Assignment');
    }
}
