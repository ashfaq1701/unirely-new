@extends('chats.partials.appended-panel')

@section('panel-heading')
	View Application
@endsection

@section('panel-body')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h2>Application To {{ $application->university->name }}</h2>
				<p>Name of the applicant: {{ $application->user->name }}</p>
				<p>Selected Plan: {{ $application->package->name }}</p>
				<p>Payment Done: {{ $application->package->payment }} USD</p>
				<div class="form-group">
					<label class="control-label" for="select-mentor">Select Mentor</label>
					<select id="select-mentor" name="select-mentor" class="form-control">
						@foreach($mentors as $mentor)
						<option value="{{ $mentor->id }}">{{ $mentor->name }}</option>
						@endforeach
					</select>
				</div>
				<a href="#" class="btn btn-default" id="assign-mentor" onclick="createMentorAssignment({{ $application->id }})">Assign Mentor</a>
			</div>
		</div>
	</div>
@endsection