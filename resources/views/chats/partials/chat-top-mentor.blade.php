<div class="col-md-6">
	Student Name: {{ $user->name }}<br>
	<br/>
	<span class="last-seen">Last Seen <span id="last-seen-{{ $channel->id }}">{{ carbonToReadableString($channel->otherUser()->last_seen_at)}}</span></span>
</div>

<div class="col-md-1 col-md-offset-1">
@if($channel->otherUser()->is_online == 1)
	<span class="ico-phone2" onclick="doCall({{ empty($channel->otherUser()->peer_id) ? 0 : "'".$channel->otherUser()->peer_id."'" }})"></span>
@endif
</div>
<div class="col-md-4">
	{{ $plan }} Plan:
	@if($difference >= 0) 
	    {{ $difference }} Days Left
	@else
	    Expired {{ abs($difference) }} Days Ago
	@endif
</div>