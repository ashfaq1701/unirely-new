<a href="#" class="media" onclick="channelSelected('{{ $adminChannel->id }}')"> 
	<span class="pull-left">
		@if(empty($adminChannel->otherUser()->profile_image))
			<img src="{{ config('constants.dummy_profile_image') }}" class="media-object img-circle" alt="" width="50px" height="50px;">
		@else
			<img src="{{ '/images/'.$adminChannel->otherUser()->profile_image }}" class="media-object img-circle" alt="" width="50px" height="50px;">
		@endif
	</span> 
	<span class="media-body">
		<span id="{{ $adminChannel->id }}-online-status" class="hasnotification hasnotification-{{ ($adminChannel->otherUser()->is_online == 1) ? 'success' : 'default' }} mr5"></span>
		<span class="media-heading contact-name">Admin</span> 
		<span {!! !empty($adminChannel->unreadMessagesCount()) ? 'class="label label-danger" ' : '' !!}id="unread-{{ $adminChannel->id }}">{{ $adminChannel->unreadMessagesCount() }}</span>
		<span class="media-text ellipsis nm" id="last-message-{{ $adminChannel->id }}">
			@if(!empty($adminChannel->lastMessage()))
				{{ $adminChannel->lastMessage()->text or 'Attachment' }}
			@else
				No message exchange found
			@endif
		</span>
		@if(!empty($adminChannel->lastMessage()))
			@if(($adminChannel->lastMessage()->type == 'file') || ($adminChannel->lastMessage()->type == 'image'))
				<span class="media-meta">
					<i class="ico-attachment"></i>
				</span>
			@endif
		@endif
		<span class="media-meta pull-right">Last Seen {{ carbonToReadableString($adminChannel->otherUser()->last_seen_at)  }}</span>
	</span>
</a>