@extends('chats.partials.appended-panel')

@section('panel-heading')
	View Application Assignment
@endsection

@section('panel-body')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h2>Application To {{ $assignment->application->university->name }}</h2>
				<p>Name of the applicant: {{ $assignment->application->user->name }}</p>
				<p>Selected Plan: {{ $assignment->application->package->name }}</p>
			</div>
		</div>
	</div>
@endsection

@section('panel-footer')
	<div class="panel-footer appended-content">
		<button class="btn btn-default" onclick="acceptAssignment({{ $assignment->id }})">Accept</button>
		<button class="btn btn-default" onclick="rejectAssignment({{ $assignment->id }})">Reject</button>
	</div>
@endsection