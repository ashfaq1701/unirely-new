<div class="panel-heading appended-content">
	<h3 class="panel-title">
		@yield('panel-heading')
	</h3>
</div>

<div class="panel-body appended-content appended-content-body">
	@yield('panel-body')
</div>


@yield('panel-footer')