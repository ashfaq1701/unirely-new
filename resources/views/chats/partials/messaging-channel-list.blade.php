@if(Auth::user()->role->name == 'admin')
	@include('chats.partials.admin-messaging-channel-list')
@elseif(Auth::user()->role->name == 'mentor')
	@include('chats.partials.mentor-messaging-channel-list')
@elseif(Auth::user()->role->name == 'student')
	@include('chats.partials.student-messaging-channel-list')
@endif