@include('chats.partials.admin-channel')

<hr />
<p class="menu-divider">
	<span>PENDING</span>
</p>
<hr />
@for ($i = 0; $i < $assignments->count(); $i++)
	<a href="#" class="media" id="assignment-container-{{ $assignments->get($i)->id }}" onclick="viewApplication({{ $assignments->get($i)->id }})"> 
		<span class="pull-left">
			@if(empty($assignments->get($i)->application->user->profile_image))
				<img src="{{ config('constants.dummy_profile_image') }}" class="media-object img-circle" alt="" width="50px" height="50px;">
			@else
				<img src="{{ '/images/'.$assignments->get($i)->application->user->profile_image }}" class="media-object img-circle" alt="" width="50px" height="50px;">
			@endif
		</span> 
		<span class="media-body">
			<span class="hasnotification hasnotification-default mr5"></span>
			<span class="media-heading contact-name">{{ $assignments->get($i)->application->user->name }}</span> 
			<span class="media-text ellipsis nm">
				Assignment needs your approval
			</span>
		</span>
	</a>
@endfor

<hr />
<p class="menu-divider">
	<span>ACTIVE</span>
</p>
<hr id="active-head"/>
@for ($i = 0; $i < $activeChannels->count(); $i++)
	<a href="#" class="media" onclick="channelSelected('{{ $activeChannels->get($i)->id }}')"> 
		<span class="pull-left">
			@if(empty($activeChannels->get($i)->otherUser()->profile_image))
				<img src="{{ config('constants.dummy_profile_image') }}" class="media-object img-circle" alt="" width="50px" height="50px;">
			@else
				<img src="{{ '/images/'.$activeChannels->get($i)->otherUser()->profile_image }}" class="media-object img-circle" alt="" width="50px" height="50px;">
			@endif
		</span> 
		<span class="media-body">
			<span id="{{ $activeChannels->get($i)->id }}-online-status" class="hasnotification hasnotification-{{ ($activeChannels->get($i)->otherUser()->is_online == 1) ? 'success' : 'default' }} mr5"></span>
			<span class="media-heading contact-name">{{ $activeChannels->get($i)->otherUser()->name }}</span> 
			<span {!! !empty($activeChannels->get($i)->unreadMessagesCount()) ? 'class="label label-danger" ' : '' !!}id="unread-{{ $activeChannels->get($i)->id }}">{{ $activeChannels->get($i)->unreadMessagesCount() }}</span>
			<span class="media-text ellipsis nm" id="last-message-{{ $activeChannels->get($i)->id }}">
				@if(!empty($activeChannels->get($i)->lastMessage()))
					{{ $activeChannels->get($i)->lastMessage()->text or 'Attachment' }}
				@else
					No message exchange found
				@endif
			</span>
			@if(!empty($activeChannels->get($i)->lastMessage()))
				@if(($activeChannels->get($i)->lastMessage()->type == 'file') || ($activeChannels->get($i)->lastMessage()->type == 'image'))
					<span class="media-meta">
						<i class="ico-attachment"></i>
					</span>
				@endif
			@endif
			<span class="media-meta pull-right">Last Seen {{ carbonToReadableString($activeChannels->get($i)->otherUser()->last_seen_at) }}</span>
		</span>
	</a>
@endfor

<hr />
<p class="menu-divider">
	<span>ARCHIEVE</span>
</p>
<hr />
@for ($i = 0; $i < $expiredChannels->count(); $i++)
	<a href="#" class="media" onclick="channelSelected('{{ $expiredChannels->get($i)->id }}')"> 
		<span class="pull-left">
			@if(empty($expiredChannels->get($i)->otherUser()->profile_image))
				<img src="{{ config('constants.dummy_profile_image') }}" class="media-object img-circle" alt="" width="50px" height="50px;">
			@else
				<img src="{{ '/images/'.$expiredChannels->get($i)->otherUser()->profile_image }}" class="media-object img-circle" alt="" width="50px" height="50px;">
			@endif
		</span> 
		<span class="media-body">
			<span class="hasnotification hasnotification-default mr5"></span>
			<span class="media-heading contact-name">{{ $expiredChannels->get($i)->otherUser()->name }}</span> 
			<span {!! !empty($expiredChannels->get($i)->unreadMessagesCount()) ? 'class="label label-danger" ' : '' !!}id="unread-{{ $expiredChannels->get($i)->id }}">{{ $expiredChannels->get($i)->unreadMessagesCount() }}</span>
			<span class="media-text ellipsis nm" id="last-message-{{ $expiredChannels->get($i)->id }}">
				@if(!empty($expiredChannels->get($i)->lastMessage()))
					{{ $expiredChannels->get($i)->lastMessage()->text or 'Attachment' }}
				@else
					No message exchange found
				@endif
			</span>
			@if(!empty($expiredChannels->get($i)->lastMessage()))
				@if(($expiredChannels->get($i)->lastMessage()->type == 'file') || ($expiredChannels->get($i)->lastMessage()->type == 'image'))
					<span class="media-meta">
						<i class="ico-attachment"></i>
					</span>
				@endif
			@endif
			<span class="media-meta pull-right">Last Seen {{ carbonToReadableString($expiredChannels->get($i)->otherUser()->last_seen_at) }}</span>
		</span>
	</a>
@endfor