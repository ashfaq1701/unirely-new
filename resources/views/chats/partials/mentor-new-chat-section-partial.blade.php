<a href="#" class="media" onclick="channelSelected('{{ $channel->id }}')"> 
	<span class="pull-left">
		@if(empty($channel->otherUser()->profile_image))
			<img src="{{ config('constants.dummy_profile_image') }}" class="media-object img-circle" alt="" width="50px" height="50px;">
		@else
			<img src="{{ '/images/'.$channel->otherUser()->profile_image }}" class="media-object img-circle" alt="" width="50px" height="50px;">
		@endif
	</span> 
	<span class="media-body">
		<span id="{{ $channel->id }}-online-status" class="hasnotification hasnotification-{{ ($channel->otherUser()->is_online == 1) ? 'success' : 'default' }} mr5"></span>
		<span class="media-heading contact-name">{{ $channel->otherUser()->name }}</span> 
		<span {!! !empty($channel->unreadMessagesCount()) ? 'class="label label-danger" ' : '' !!}id="unread-{{ $channel->id }}">{{ $channel->unreadMessagesCount() }}</span>
		<span class="media-text ellipsis nm">
			@if(!empty($channel->lastMessage()))
				{{ $channel->lastMessage()->text or 'Attachment' }}
			@else
				No message exchange found
			@endif
		</span>
		@if(!empty($channel->lastMessage()))
			@if(($channel->lastMessage()->type == 'image') || ($channel->lastMessage()->type == 'file'))
				<span class="media-meta">
					<i class="ico-attachment"></i>
				</span>
			@endif
		@endif
		<span class="media-meta pull-right">Last Seen {{ carbonToReadableString($channel->otherUser()->last_seen_at) }}</span>
	</span>
</a>