<div class="col-md-5">
    Member Name: {{ $user->name }}<br>
    Role: {{ ucfirst($user->role->name) }} 
    @if($user->role->name == 'mentor')
    	({{ $user->university->name }})
    @endif
    <br/>
	<span class="last-seen">Last Seen <span id="last-seen-{{ $channel->id }}">{{ carbonToReadableString($channel->otherUser()->last_seen_at)}}</span></span>
</div>

<div class="col-md-1">
@if($channel->otherUser()->is_online == 1)
	<span class="ico-phone2" onclick="doCall({{ empty($channel->otherUser()->peer_id) ? 0 : "'".$channel->otherUser()->peer_id."'" }})"></span>
@endif
</div>