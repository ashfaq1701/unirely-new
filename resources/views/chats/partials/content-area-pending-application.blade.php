@extends('chats.partials.appended-panel')

@section('panel-heading')
	View Application Assignment
@endsection

@section('panel-body')
	<h2>Application To {{ $application->university->name }}</h2>
	<p>Selected Plan: {{ $application->package->name }}</p>
	<p>Payment Done: {{ $application->package->payment }} USD</p>
	<p>Payment Date: {{ $application->createdAt }}</p>
	<h3>Status: </h3>
	<p>
		@if($application->assignments->count() > 0)
			Your application is assigned to a mentor and awaiting for his acceptence.
		@else
			Your application is waiting for admin assignment to a specific mentor.
		@endif
	</p>
@endsection