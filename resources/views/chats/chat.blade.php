@extends('layouts.content-without-sidebar')

@section('page-title')
	Messages
@endsection

@section('custom-css')
	<link rel="stylesheet" href="{{ asset('assets/css/chat.css') }}">
@endsection

@section('main-content')
<!-- message list-->
<!-- START Table layout -->
<div class="col-lg-4 valign-top panel panel-minimal">
	<!-- panel heading -->
	<div class="panel-heading">
		<!-- panel toolbar -->
		<div class="panel-toolbar">
			<div class="input-group">
				<div class="has-icon">
					<input type="text" class="form-control" id="search-contacts" placeholder="Search Contacts..."> 
					<i class="ico-search form-control-icon"></i>
				</div>
			</div>
		</div>
	</div>
	<!--/ panel heading -->

	<hr class="mt10 mb10">
	<!-- horizontal line -->
	<!-- panel body -->
	<div class="panel-body np">
		<!-- message list -->
		<div class="media-list chat-contact-list">
			@include('chats.partials.messaging-channel-list')
		</div>
		<!-- message list -->
	</div>
	<!--/ panel body -->
</div>
<!--/ message list -->

<!-- message content -->
<div class="col-lg-8 valign-top panel panel-primary right-section">
	<!-- panel heading -->
	<div class="panel-heading chat-container">
		@include('chats.partials.content-area-head')
	</div>
	<!--/ panel heading -->

	<!-- panel body -->
	<div class="panel-body np chat-container" id="chats-content">
		@include('chats.partials.content-area-chat')
	</div>
	<!-- panel body -->

	<!-- panel footer -->
	<div class="panel-footer chat-container" id="chats-footer">
		@include('chats.partials.content-area-footer')
	</div>
	<!-- panel footer -->
</div>
@endsection