@extends('layouts.content-without-sidebar')

@section('page-title')
	Checkout
@endsection

@section('main-content')
	@include('payments.partials.details')
	@include('payments.partials.payment-form')
@endsection

@section('custom-scripts')
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript">
  		Stripe.setPublishableKey("{{ config('services.stripe.key') }}");
	</script>
@endsection