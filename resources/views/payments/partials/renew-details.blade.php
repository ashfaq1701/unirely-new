<p><strong>You will be charged for the following orders,</strong></p>
<div class="row">
	<div class="col-md-12">
		<strong>University Name: </strong>{{ $channel->subscription->application->university->name }}
	</div>
	<br/>
	<div class="col-md-12">
		<strong>Plans: </strong>
	</div>
	@foreach($packages as $package)
		<div class="col-md-4 align-center">
			<input type="radio" class="package-input" name="package-input" data-id="{{ $package->id }}"/><br/>
			<span class="package-name"><strong>{{ $package->name }}</strong></span><br/>
			<span class="package-validity"><strong>Validity: </strong>{{ $package->validity }} Days</span><br/>
			<span class="package-price"><strong>Price: </strong>{{ $package->payment }} USD</span>
		</div>
	@endforeach
</div>