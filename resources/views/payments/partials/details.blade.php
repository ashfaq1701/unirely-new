<p><strong>You will be charged for the following orders,</strong></p>
<table class="table table-bordered">
	<thead>
		<tr>
			<th>University Name</th>
			<th>Plan Name</th>
			<th>Price</th>
		</tr>
	</thead>
	<tbody>
		@foreach($details as $detail)
			<tr>
				<td>{{ $detail['university_name'] }}</td>
				<td>{{ $detail['package_name'] }}</td>
				<td>{{ $detail['price'] }}</td>
			</tr>
		@endforeach
		<tr>
			<td><strong>Total</strong></td>
			<td></td>
			<td>{{ $totalPrice }}</td>
		</tr>
	</tbody>
</table>