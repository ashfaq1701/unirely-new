<div class="row payment-form-container">
	<form action="" method="POST" 
	@if(empty(Auth::user()->stripe_id))
		id="payment-form" 
	@endif
	class="form-horizontal" role="form">
  		<span class="payment-errors"></span>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="package" id="package" value="1">
		@if(empty(Auth::user()->stripe_id))
  			<div class="form-group">
    			<label class="control-label col-md-2" for="number">Card Number</label>
    			<div class="col-md-9">
      				<input type="text" data-stripe="number" class="form-control">
      			</div>
  			</div>

  			<div class="form-group">
    			<label class="control-label col-md-2" for="exp_month">Expiration (MM/YY)</label>
      			<div class="col-md-4">
      				<input type="text" data-stripe="exp_month" class="form-control">
      			</div>
      			<div class="col-md-1">
    				<span > / </span>
    			</div>
    			<div class="col-md-4">
    				<input type="text" data-stripe="exp_year" class="form-control">
    			</div>
  			</div>

  			<div class="form-group">
    			<label class="control-label col-md-2" for="cvc">CVC</label>
      			<div class="col-md-9">
      				<input type="text" data-stripe="cvc" class="form-control">
    			</div>
  			</div>
  		@endif
		<div class="col-md-2 col-md-offset-5">
  			<input type="submit" class="submit btn btn-default" value="Submit Payment">
  		</div>
	</form>
</div>