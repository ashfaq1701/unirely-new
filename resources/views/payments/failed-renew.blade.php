@extends('layouts.content-without-sidebar')

@section('page-title')
	Renewal Attempt Failed
@endsection

@section('main-content')
	<p>
		Your payment attempts failed. Please visit the renew page to try again.<br/>
		{{ $e->getMessage() }}
	</p>
@endsection