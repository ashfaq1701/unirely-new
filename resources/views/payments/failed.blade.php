@extends('layouts.content-without-sidebar')

@section('page-title')
	Payment Failed
@endsection

@section('main-content')
	<p>
		Your payment attempts failed. Please visit the checkout page to try again.<br/>
		{{ $e->getMessage() }}
	</p>
	<p>
		<a href="/checkout">Checkout</a>
	</p>
@endsection