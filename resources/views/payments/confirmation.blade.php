@extends('layouts.content-without-sidebar')

@section('page-title')
	Payment Successful
@endsection

@section('main-content')
	<p><strong>You subscribed for the following packages. It's under review for a while. Site admin will assign you a mentor shortly.</strong></p>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>University Name</th>
				<th>Plan Name</th>
			</tr>
		</thead>
		<tbody>
			@foreach($details as $detail)
				<tr>
					<td>{{ $detail['university_name'] }}</td>
					<td>{{ $detail['package_name'] }}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection