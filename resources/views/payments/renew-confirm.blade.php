@extends('layouts.content-without-sidebar')

@section('page-title')
	Plan Successfully Renewed
@endsection

@section('main-content')
	<p><strong>You renewed the following package. It's under review for a while. Site admin will assign you a mentor shortly.</strong></p>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>University Name</th>
				<th>Plan Name</th>
				@if(isset($detail['mentor']))
					<th>Mentor Name</th>
				@endif
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>{{ $detail['university'] }}</td>
				<td>{{ $detail['package'] }}</td>
				@if(isset($detail['mentor']))
					<td>{{ $detail['mentor'] }}</td>
				@endif
			</tr>
		</tbody>
	</table>
@endsection