<!DOCTYPE html>
<html>
    <!-- START Head -->
    <head>
        <!-- START META SECTION -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Adminre - 1.0.0</title>
        <meta name="description" content="Adminre admin dashboard">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
		<meta name="csrf-token" content="{{ csrf_token() }}"

        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/image/touch/apple-touch-icon-144x144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/image/touch/apple-touch-icon-114x114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/image/touch/apple-touch-icon-72x72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/image/touch/apple-touch-icon-57x57-precomposed.png">
        <link rel="shortcut icon" href="assets/image/touch/apple-touch-icon.png">
        <!--/ END META SECTION -->

        <!-- START STYLESHEETS -->
        <!-- 3rd party plugin stylesheet : optional(per use) -->
        <link rel="stylesheet" href="{{ asset('assets/css/selectize.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/jquery.gritter.min.css') }}">
        <!--/ 3rd party plugin stylesheet -->

        <!-- Library stylesheet : mandatory -->
        <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/dataTables.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
        <!--/ Library stylesheet -->

        <!-- Application stylesheet : mandatory -->
        <link rel="stylesheet" href="{{ asset('assets/css/layout.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/uielement.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
        <!--/ Application stylesheet -->
        <!-- END STYLESHEETS -->

		@yield('custom-css')

        <!-- START JAVASCRIPT SECTION - Load only modernizr script here -->
        <script src="{{ asset('assets/js/modernizr.min.js') }}"></script>
        <!--/ END JAVASCRIPT SECTION -->
    </head>
    <!--/ END Head -->
    <body>
        @yield('all')

        <!-- START JAVASCRIPT SECTION (Load javascripts at bottom to reduce load time) -->
        <!-- Library script : mandatory -->
        <script type="text/javascript" src="{{ asset('assets/js/jquery.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery-ui-touch.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery-migrate.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/core.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/init.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/selectize.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.gritter.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/bootbox.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/page.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/peer.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/constants.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/functions.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/util.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery-ajax-fileupload.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/modernizr-custom.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/socket.js') }}"></script>
		<script>window.appSocket = new Socket("{{ config('socket.websocket_path') }}");</script>
        <script type="text/javascript" src="{{ asset('assets/js/util.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/custom.js') }}"></script>
        
        @yield('custom-scripts')
        <!--/ 3rd party plugin script -->
        <!--/ END JAVASCRIPT SECTION -->
    </body>
</html>