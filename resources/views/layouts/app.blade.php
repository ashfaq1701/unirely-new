@extends('layouts.base')

@section('all')
<!-- START Template Header -->
<header id="header" class="navbar navbar-fixed-top">
	<!-- START navbar header -->
	<div class="navbar-header">
		<!-- Brand -->
		<a class="navbar-brand" href="index.html"> <span class="template-logo"></span>
		</a>
		<!--/ Brand -->
	</div>
	<!--/ END navbar header -->

	<!-- START Toolbar -->
	<div class="navbar-toolbar clearfix">
		@include('partials.left-nav-bar')

		<!-- START Right nav -->
		<ul class="nav navbar-nav navbar-right">
			<!-- Authentication Links -->
			@if (Auth::guest())
				<li class="dropdown profile">
					<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> 
						<span class="meta">
							<span class="text hidden-xs hidden-sm">Login / Register</span> 
							<span class="arrow"></span>
						</span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="{{ url('/login') }}">
								<span class="icon">
									<i class="ico-exit"></i>
								</span> 
								Login
							</a>
						</li>
						<li>
							<a href="{{ url('/register') }}">
								<span class="icon">
									<i class="ico-exit"></i>
								</span> 
								Register
							</a>
						</li>
					</ul>
				</li> 
			@else
				<!-- Profile dropdown -->
				<li class="dropdown profile">
					<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> 
						<span class="meta">
							<span class="avatar">
								@if(empty(Auth::user()->profile_image))
									<img src="{{ config('constants.dummy_profile_image') }}" class="img-circle" alt="" />
								@else
									<img src="{{ '/images/'.Auth::user()->profile_image }}" class="img-circle" alt="" />
								@endif
							</span> 
							<span class="text hidden-xs hidden-sm">{{ Auth::user()->name }}</span> 
							<span class="arrow"></span>
						</span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<li class="pa15">
							<h5 class="semibold hidden-xs hidden-sm">
								60% <br /> 
								<small class="text-muted">Profile complete</small>
							</h5>
							<h5 class="semibold hidden-md hidden-lg">
								{{ Auth::user()->name }}<br /> 
								<small class="text-muted">60% Profile complete</small>
							</h5>
							<div class="progress progress-xs nm mt10">
								<div class="progress-bar progress-bar-warning" style="width: 60%">
									<span class="sr-only">60% Complete</span>
								</div>
							</div>
						</li>
						<li class="divider"></li>
						@foreach($dropdownMenu as $name=>$item)
							@if(in_array(Auth::user()->role->name, $item['roles']))
								<li>
									<a href="/{{ $item['url'] }}">
										<span class="icon">
											<i class="{{ $item['icon'] }}"></i>
										</span>
										{{ $name }}
									</a>
								</li>
							@endif
						@endforeach
						<li class="divider"></li>
						<li>
							<a href="{{ url('/logout') }}">
								<span class="icon">
									<i class="ico-exit"></i>
								</span> 
								Sign Out
							</a>
						</li>
					</ul>
				</li>
				<!--/ Profile dropdown -->
			@endif
		</ul>
		<!--/ END Right nav -->
	</div>
	<!--/ END Toolbar / link -->
</header>
<!--/ END Template Header -->

@yield('content')

@if(!Auth::guest())
	@include('partials.call-modal')
@endif

@if(!Auth::guest() && (Auth::user()->role->name == 'student'))
	@include('partials.university-select-modal')
@endif

@endsection

@section('custom-scripts')
	@if(!Auth::guest())
		<script type="text/javascript" src="{{ asset('assets/js/chat.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/call-ui.js') }}"></script>
		<script type="text/javascript" src="{{ asset('assets/js/peer-custom.js') }}"></script>
	@endif
@endsection

