@extends('layouts.app')

@section('content')
	@include('partials.sidebar')
	@include('partials.content-area')
@endsection
