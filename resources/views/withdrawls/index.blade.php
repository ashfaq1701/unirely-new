@extends('layouts.content-without-sidebar')

@section('page-title')
	Withdrawls
@endsection

@section('main-content')
	@include('withdrawls.partials.perform-withdrawl')
	@include('withdrawls.partials.mentorships')
	@include('withdrawls.partials.payments')
@endsection