<row>
	@if (session('status'))
    	<div class="alert alert-success">
        	{{ session('status') }}
    	</div>
	@endif
	@if (session('warning'))
    	<div class="alert alert-warning">
        	{{ session('warning') }}
    	</div>
    @endif
	<div class="col-md-5 col-md-offset-7 bottom-gap">
		<h2 class="right-float">Available Balance : {{ $balance }} USD</h2>
	</div>
	<form role="form" action="/withdrawls/request" method="POST">
		{{ csrf_field() }}
		<div class="form-group">
			<div class="col-md-1 bottom-gap">
				<label for="amount">Amount: </label>
			</div>
			<div class="col-md-6 bottom-gap">
				<input type="text" class="form-control" id="amount" name="amount"/>
			</div>
		</div>
		<div class="col-md-5 bottom-gap">
			<button type="submit" class="btn btn-default">Request Payment</button>
		</div>
	</form>
</row>