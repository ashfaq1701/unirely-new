<h2 class="bottom-gap">Your payments</h2>
<div class="row">
	<form action="/withdrawls/{{$user->id}}" method="POST">
		{{ csrf_field() }}
		<div class="col-md-8">
			<div class="form-group">
				<div class="row">
                	<label class="col-sm-2 control-label">Select date range</label>
                	<div class="col-sm-8">
                    	<div class="row">
                        	<div class="col-md-6">
                        		<input type="text" class="form-control datepicker" id="withdrawl-from" name="withdrawl-from" placeholder="From" />
                        	</div>
                           	<div class="col-md-6">
                           		<input type="text" class="form-control datepicker" id="withdrawl-to" name="withdrawl-to" placeholder="To" />
                           	</div>
                    	</div>
                    </div>
                    <div class="col-sm-2">
                    	<button class="btn btn-default" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>
	</form>
	<div class="col-md-12">
		<table class="datatable table table-striped">
			<thead>
				<tr>
					<th>Date</th>
					<th>Status</th>
					<th>Amount</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>Total</th>
					<th></th>
					<th>{{ $totalWithdrawl }}</th>
				</tr>
			</tfoot>
			<tbody>
				@foreach($withdrawls as $withdrawl)
					<tr>
						<td>{{ $withdrawl->created_at }}</td>
						<td>{{ ucfirst($withdrawl->status) }}</td>
						<td>{{ $withdrawl->amount }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>