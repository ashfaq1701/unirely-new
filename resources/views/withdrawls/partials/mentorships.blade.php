<h2 class="bottom-gap">Your payables for mentorships</h2>
<div class="row">
	<form action="/withdrawls/{{$user->id}}" method="POST">
		{{ csrf_field() }}
		<div class="col-md-8">
			<div class="form-group">
				<div class="row">
                	<label class="col-sm-2 control-label">Select date range</label>
                	<div class="col-sm-8">
                    	<div class="row">
                        	<div class="col-md-6">
                        		<input type="text" class="form-control datepicker" id="mentorship-from" name="mentorship-from" placeholder="From" />
                        	</div>
                           	<div class="col-md-6">
                           		<input type="text" class="form-control datepicker" id="mentorship-to" name="mentorship-to" placeholder="To" />
                           	</div>
                    	</div>
                    </div>
                    <div class="col-sm-2">
                    	<button class="btn btn-default" type="submit">Submit</button>
                    </div>
                </div>
            </div>
        </div>
	</form>
	<div class="col-md-12">
		<table class="datatable table table-striped">
			<thead>
				<tr>
					<th>Student Name</th>
					<th>Plan</th>
					<th>End Date</th>
					<th>Payment</th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th>Total</th>
					<th></th>
					<th></th>
					<th>{{ $totalPayable }}</th>
				</tr>
			</tfoot>
			<tbody>
				@foreach($mentorships as $mentorship)
					<tr>
						<td>{{ $mentorship->user->name }}</td>
						<td>{{ $mentorship->application->package->name }}</td>
						<td>{{ $mentorship->valid_till }}</td>
						<td>{{ $mentorship->application->package->payment * $user->payment_factor }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>