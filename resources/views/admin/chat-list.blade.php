@extends('layouts.content-with-sidebar')

@section('page-title')
	Chat List
@endsection

@section('main-content')
	<p><strong>Email</strong> : {{ $user->email }}</p>
	<p><strong>Name</strong> : {{ $user->name }}</p>
	<p><strong>Role</strong> : {{ ucfirst($user->role->name) }}</p>
	<table class="table table-striped datatable">
		<thead>
			<tr>
				<th>{{ $user->role->name == 'mentor' ? 'Student' : 'Mentor' }} Email</th>
				<th>{{ $user->role->name == 'mentor' ? 'Student' : 'Mentor' }} Name</th>
				<th>Expiration Date</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($channels as $channel)
				<tr>
					<td>{{ $channel->otherThanGivenUser($user)->email }}</td>
					<td>{{ $channel->otherThanGivenUser($user)->name }}</td>
					<td>{{ !empty($channel->subscription) ? $channel->subscription->valid_till : ' - ' }}</td>
					<td>
						<a href="#" onclick="viewChatsAdmin({{ $channel->id }})">
							<span class="ico-bubbles10"></span>
						</a>
					</td>
				</tr>	
			@endforeach
		</tbody>
	</table>
	@include('admin.partials.channel-messages-modal')
@endsection