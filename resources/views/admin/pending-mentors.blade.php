@extends('layouts.content-with-sidebar')

@section('page-title')
	Pending Mentors
@endsection

@section('main-content')
	<table class="table table-striped datatable">
		<thead>
			<tr>
				<th>Name</th>
				<th>University</th>
				<th>Email</th>
				<th>Phone</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($mentors as $mentor)
				<tr>
					<td>{{ $mentor->name }}</td>
					<td>{{ $mentor->university->name }}</td>
					<td>{{ $mentor->email }}</td>
					<td>{{ $mentor->phone }} </td>
					<td>
						<a href="/profile/{{ $mentor->id }}">
							<span class="ico-profile"></span>
						</a>
						<a href="#" onclick="approveMentor({{ $mentor->id }})">
							<span class="ico-ok-circle"></span>	
						</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection