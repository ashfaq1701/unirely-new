<div class="modal fade bs-example-modal-lg" id="view-chats" tabindex="-1" role="dialog" aria-labelledby="viewChatsLabel">
	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="viewChatsLabel">View Chats</h4>
      		</div>
      		<div class="modal-body">
      			@include('chats.partials.content-area-chat')
      		</div>
      		<div class="modal-footer">
      			<div class="input-group" style="width: 100%;">
					<span class="input-group-btn">
						<label class="btn btn-primary">
    						<i class="ico-attachment"></i>Attach
    						<input type="file" id="chat_file_input" style="display: none;">
						</label>
					</span>
					<input type="text" class="form-control chat-input-text__field" placeholder="Type your message"> 
					<span class="input-group-btn">
						<button class="btn btn-primary" type="button" id="send-message">
							<i class="ico-paper-plane"></i> <span class="semibold">Send</span>
						</button>
					</span>
				</div>
      		</div>
      	</div>
    </div>
</div>