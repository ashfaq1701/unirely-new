@extends('layouts.content-with-sidebar')

@section('page-title')
	Universities
@endsection

@section('main-content')
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="panel-toolbar text-right">
        		<div class="btn-group">
            		<a class="btn btn-primary" href="/universities?active=1">Active</a>
            		<a class="btn btn-primary" href="/universities?active=0">Inactive</a>
    			</div>
			</div>
		</div>
		<div class="panel-body">
			<table class="table table-striped datatable">
				<thead>
					<tr>
						<th>Name</th>
						<th>Status</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($universitiesTable as $university)
						<tr>
							<td>{{ $university->name }}</td>
							<td>{{ $university->approved == 1 ? 'Approved' : 'Pending' }}
							<td>
								@if($university->approved == 1)
									<a href="#" onclick="banUniversity({{ $university->id }})">
										<span class="ico-ban-circle"></span>	
									</a>
								@else
									<a href="#" onclick="approveUniversity({{ $university->id }})">
										<span class="ico-ok-circle"></span>	
									</a>
								@endif
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endsection