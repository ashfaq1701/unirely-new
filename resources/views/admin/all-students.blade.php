@extends('layouts.content-with-sidebar')

@section('page-title')
	All Students
@endsection

@section('main-content')
	<table class="table table-striped datatable">
		<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($students as $student)
				<tr>
					<td>{{ $student->name }}</td>
					<td>{{ $student->email }}</td>
					<td>{{ $student->phone }} </td>
					<td>
						<a href="/profile/{{ $student->id }}">
							<span class="ico-profile"></span>
						</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection