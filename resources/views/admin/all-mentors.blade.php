@extends('layouts.content-with-sidebar')

@section('page-title')
	All Mentors
@endsection

@section('main-content')
	<table class="table table-striped datatable">
		<thead>
			<tr>
				<th>Name</th>
				<th>University</th>
				<th>Email</th>
				<th>Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			@foreach($mentors as $mentor)
				<tr>
					<td>{{ $mentor->name }}</td>
					<td>{{ $mentor->university->name }}</td>
					<td>{{ $mentor->email }}</td>
					<td>{{ ($mentor->active == 1) ? 'Active' : 'Inactive' }}</td>
					<td>
						<a href="/profile/{{ $mentor->id }}">
							<span class="ico-profile"></span>
						</a>
						@if($mentor->active == 0)
							<a href="#" onclick="approveMentor({{ $mentor->id }})">
								<span class="ico-ok-circle"></span>	
							</a>
						@else
							<a href="#" onclick="banMentor({{ $mentor->id }})">
								<span class="ico-ban-circle"></span>	
							</a>
						@endif
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endsection