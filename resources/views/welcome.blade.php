@extends('layouts.content-without-sidebar')

@section('page-title')
	Landing Page
@endsection

@section('main-content')
<section id="main" role="main">
	<!-- START Template Container -->
	<section class="container">
		<!-- START row -->
		<div class="row">
			<div class="col-lg-4 col-lg-offset-4">
				Your applications landing page.
			</div>
		</div>
	</section>
</section>
@endsection
