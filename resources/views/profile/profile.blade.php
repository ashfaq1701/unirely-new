@extends('layouts.content-without-sidebar')

@section('page-title')
	Profile
@endsection

@section('main-content')
<div class="container">
	<div class="row">
		@if((Auth::user()->role->name == 'admin') && ($user->role->name != 'admin'))
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-2 col-md-offset-10">
						<a href="/user/chat-list/{{ $user->id }}" class="btn btn-success">View Chats</a>
					</div>
				</div>
			</div>
		@endif
		<!-- left column -->
		<div class="col-md-3">
			<div class="text-center">
				@if(empty($user->profile_image))
					<img src="{{ config('constants.dummy_profile_image') }}" class="avatar img-circle" width="125px" height="125px" alt="avatar">
				@else
					<img src="{{ '/images/'.$user->profile_image }}" class="avatar img-circle" width="125px" height="125px" alt="avatar">
				@endif
				<h6>Upload a different photo...</h6>
          		<input type="file" class="form-control" name="profile-picture" id="profile-picture">
        	</div>
		</div>
      
		<!-- edit form column -->
		<div class="col-md-9 personal-info">
			@if (session('status'))
    			<div class="alert alert-success">
        			{{ session('status') }}
    			</div>
			@endif
			@if (session('warning'))
    			<div class="alert alert-warning">
        			{{ session('warning') }}
    			</div>
			@endif
			<h3>Personal info</h3>
        
			<form class="form-horizontal" role="form" 
			@if((Auth::user()->id == $user->id) || (Auth::user()->role->name == 'admin'))
				action="/profile/save{{ (Auth::user()->role->name == 'admin') ? '/'.$user->id : '' }}" method="POST" 
			@endif
			id="profile-form">
				{{ csrf_field() }}
				<input type="hidden" id="profile-image" name="profile-image" val=""/>
				<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
					@if ($errors->has('name')) 
						<span class="help-block col-lg-offset-2 col-lg-10"> 
							<strong>{{$errors->first('name') }}</strong>
						</span> 
					@endif 
					<label class="col-lg-3 control-label">Name:</label>
					<div class="col-lg-8">
						<input class="form-control" name="name" type="text" value="{{ $user->name or '' }}">
					</div>
				</div>
				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
					@if ($errors->has('email')) 
						<span class="help-block col-lg-offset-2 col-lg-10"> 
							<strong>{{$errors->first('email') }}</strong>
						</span> 
					@endif 
					<label class="col-lg-3 control-label">Email:</label>
					<div class="col-lg-8">
						<input class="form-control" name="email" type="text" value="{{ $user->email or '' }}">
					</div>
				</div>
				<div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
					@if ($errors->has('phone')) 
						<span class="help-block col-lg-offset-2 col-lg-10"> 
							<strong>{{$errors->first('phone') }}</strong>
						</span> 
					@endif 
					<label class="col-md-3 control-label">Phone:</label>
					<div class="col-md-8">
						<input class="form-control" name="phone" type="text" value="{{ $user->phone or '' }}">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">City:</label>
					<div class="col-md-8">
						<input class="form-control" name="city" type="text" value="{{ $user->userMeta->city or '' }}">
					</div>
				</div>
				@if($user->role->name == 'mentor')
					<div class="form-group">
						<label class="col-md-3 control-label">University:</label>
						<div class="col-md-8">
							<select class="form-control" name="university">
								@foreach($universities as $university)
									<option value="{{ $university->id }}" {{ (($user->university->id) == $university->id) ? 'selected' : '' }}>{{ $university->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
				@endif
				@if($user->role->name == 'student')
          			<div class="form-group">
						<label class="col-md-3 control-label">Percentage or Grade (In 12th):</label>
						<div class="col-md-8">
							<input class="form-control" type="text" name="percentage" value="{{ $user->userMeta->percentage or '' }}">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Courses Interested</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="courses-interested" name="courses-interested">
						</div>
					</div>
				@endif
				<script type="text/javascript">
					var courses = JSON.parse('{!! json_encode($user->courses) !!}');
					var userId = {{ Auth::user()->id }};
				</script>
				@if((Auth::user()->id == $user->id) || Auth::user()->role->name == 'admin')
					<div class="form-group">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-8">
							<button type="submit" class="btn btn-primary" id="profile-save">Save</button>
							<span></span>
							<input type="reset" class="btn btn-default" value="Cancel">
            			</div>
          			</div>
          		@endif
			</form>
		</div>
	</div>
</div>
@endsection

@section('custom-scripts')
	<script type="text/javascript" src="{{ asset('assets/js/profile.js') }}"></script>
@endsection