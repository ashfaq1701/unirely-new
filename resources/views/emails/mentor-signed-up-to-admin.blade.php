<h2>Unirely : New Mentor Signed Up</h2>

<p>
	A new mentor named <strong>{{ $user->name }}</strong> signed up in your website for <strong>{{ $user->university->name }}</strong> with email <a href="mailto:{{ $user->email }}"><strong>{{ $user->email }}</strong></a>.
</p>