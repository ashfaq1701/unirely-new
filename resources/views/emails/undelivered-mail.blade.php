<h2>Unirely : New Message Received</h2>

<p>
	You received a new email From {{ $messageObj->user->name }}
	@if($messageObj->user->role->name == 'mentor')
		{{ ' - '. $messageObj->user->university->name }}
	@endif
</p>

@if($messageObj->type == 'text')
<p>
	<strong>Text : </strong>
	{{ $messageObj->text }}
</p>
@elseif($messageObj->type == 'image')
<p>
	<strong>URL : </strong>
	<a href="{{ $messageObj->file_path }}">{{ basename($messageObj->file_path) }}</a>
</p>
<img style="height: 400px; width: 400px;" src="{{ $messageObj->file_path }}"/>

@elseif($messageObj->type == 'file')
<p>
	<strong>URL : </strong>
	<a href="{{ $messageObj->file_path }}">{{ basename($messageObj->file_path) }}</a>
</p>
@endif
