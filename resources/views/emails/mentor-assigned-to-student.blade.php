<h2>Unirely : Mentor Assignment</h2>

<p>
	You are assigned to a <strong>{{ $assignment->application->package->name }} plan</strong> to a student named <strong>{{ $assignment->application->user->name }}</strong>. Please accept it on your Unirely account.
</p>