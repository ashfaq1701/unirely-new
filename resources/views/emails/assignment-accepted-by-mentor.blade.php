<h2>Unirely : Assignment Accepted By Mentor</h2>

<p>
	You are now connected with {{ $assignment->user->name }}, mentor of {{ $assignment->user->university->name }}, for your subscribed {{ $assignment->application->package->name }} plan. This will expire on <strong>{{ $assignment->application->subscription->valid_till }}</strong>.
</p>