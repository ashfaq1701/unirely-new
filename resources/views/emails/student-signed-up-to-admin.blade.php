<h2>Unirely : New Student Signed Up</h2>

<p>
	A new student named <strong>{{ $user->name }}</strong> signed up in your website with email <a href="mailto:{{ $user->email }}"><strong>{{ $user->email }}</strong></a>.
</p>