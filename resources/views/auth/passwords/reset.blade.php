@extends('layouts.base')

@section('all')
<!-- START Template Main -->
<section id="main" role="main">
	<!-- START Template Container -->
	<section class="container">
		<!-- START row -->
		<div class="row">
			<div class="col-lg-4 col-lg-offset-4">
				<!-- Brand -->
				<div class="text-center" style="margin-bottom: 40px;">
					<span class="template-logo template-logo-inverse"></span>
					<h5 class="semibold text-muted mt-10">Reset your password</h5>
				</div>
				<!--/ Brand -->

				<hr>
				<!-- horizontal line -->

				@if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
				<form class="panel" role="form" method="POST" action="{{ url('/password/reset') }}">
					{{ csrf_field() }}
					<input type="hidden" name="token" value="{{ $token }}">
					<div class="panel-body">
						<div class="form-group">
							<div class="form-stack has-icon clearfix{{ $errors->has('email') ? ' has-error' : '' }}">
								@if ($errors->has('email')) 
									<span class="help-block"> 
										<strong>{{$errors->first('email') }}</strong>
									</span> 
								@endif 
								<input id="email" name="email" type="email" class="form-control input-lg" placeholder="Email" value="{{ $email or old('email') }}"> 
								<i class="ico-user2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix{{ $errors->has('password') ? ' has-error' : '' }}">
								@if ($errors->has('password')) 
									<span class="help-block"> 
										<strong>{{$errors->first('password') }}</strong>
									</span> 
								@endif 
								<input id="password" name="password" type="password" class="form-control input-lg" placeholder="Password"> 
								<i class="ico-user2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
								@if ($errors->has('password_confirmation')) 
									<span class="help-block"> 
										<strong>{{$errors->first('password_confirmation') }}</strong>
									</span> 
								@endif 
								<input id="password-confirm" name="password_confirmation" type="password" class="form-control input-lg" placeholder="Re Enter Password"> 
								<i class="ico-user2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
                        	<button type="submit" class="btn btn-primary">
                            	<i class="fa fa-btn fa-envelope"></i> Reset Password
                            </button>
                    	</div>
					</div>
				</form>
			</div>
		</div>
	</section>
@endsection