@extends('layouts.content-without-sidebar')

@section('page-title')
	Reset Password
@endsection

@section('main-content')
<form class="panel" role="form" method="POST" action="{{ url('/reset/password') }}">
	{{ csrf_field() }}
	<div class="panel-body">
		@if (session('status'))
    		<div class="alert alert-success">
        		{{ session('status') }}
    		</div>
		@endif
		@if (session('warning'))
    		<div class="alert alert-warning">
        		{{ session('warning') }}
    		</div>
		@endif
		<div class="form-group">
			<div class="form-stack has-icon clearfix{{ $errors->has('password') ? ' has-error' : '' }}">
				@if ($errors->has('password')) 
					<span class="help-block"> 
						<strong>{{$errors->first('password') }}</strong>
					</span> 
				@endif 
				<input id="password" name="password" type="password" class="form-control input-lg" placeholder="Password"> 
				<i class="ico-user2 form-control-icon"></i>
			</div>
		</div>
		<div class="form-group">
			<div class="form-stack has-icon clearfix{{ $errors->has('new_password') ? ' has-error' : '' }}">
				@if ($errors->has('new_password')) 
					<span class="help-block"> 
						<strong>{{$errors->first('new_password') }}</strong>
					</span> 
				@endif 
				<input id="new_password" name="new_password" type="password" class="form-control input-lg" placeholder="New Password"> 
				<i class="ico-key4 form-control-icon"></i>
			</div>
		</div>
		<div class="form-group">
			<div class="form-stack has-icon clearfix{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
				@if ($errors->has('new_password_confirmation')) 
					<span class="help-block"> 
						<strong>{{$errors->first('new_password_confirmation') }}</strong>
					</span> 
				@endif 
				<input id="new_password_confirmation" name="new_password_confirmation" type="password" class="form-control input-lg" placeholder="Repeat New Password"> 
				<i class="ico-key4 form-control-icon"></i>
			</div>
		</div>
		<div class="form-group">
    		<button type="submit" class="btn btn-primary">
        		<i class="fa fa-btn fa-envelope"></i> Reset Password
        	</button>
		</div>
	</div>
</form>
@endsection