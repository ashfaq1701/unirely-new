@extends('layouts.base')

@section('all')
<!-- START Template Main -->
<section id="main" role="main">
	<!-- START Template Container -->
	<section class="container">
		<!-- START row -->
		<div class="row">
			<div class="col-lg-4 col-lg-offset-4">
				<!-- Brand -->
				<div class="text-center" style="margin-bottom: 40px;">
					<span class="template-logo template-logo-inverse"></span>
					<h5 class="semibold text-muted mt-10">Register for a new account.</h5>
				</div>
				<!--/ Brand -->

				<hr>
				<!-- horizontal line -->

				<!-- Login form -->
				<form class="panel" role="form" method="POST" action="{{ url('/register') }}">
					{{ csrf_field() }}
					<div class="panel-body">
						<div class="form-group">
							<div class="form-stack has-icon clearfix{{ $errors->has('name') ? ' has-error' : '' }}">
								@if ($errors->has('name')) 
									<span class="help-block"> 
										<strong>{{$errors->first('name') }}</strong>
									</span> 
								@endif 
								<input name="name" type="text" class="form-control input-lg" placeholder="Name"> 
								<i class="ico-user2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix{{ $errors->has('email') ? ' has-error' : '' }}">
								@if ($errors->has('email')) 
									<span class="help-block"> 
										<strong>{{$errors->first('email') }}</strong>
									</span> 
								@endif 
								<input name="email" type="text" class="form-control input-lg" placeholder="Email"> 
								<i class="ico-user2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix{{ $errors->has('password') ? ' has-error' : '' }}">
								@if ($errors->has('password')) 
									<span class="help-block"> 
										<strong>{{$errors->first('password') }}</strong>
									</span> 
								@endif 
								<input name="password" type="password" class="form-control input-lg" placeholder="Password"> 
								<i class="ico-lock2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
								@if ($errors->has('password_confirmation')) 
									<span class="help-block"> 
										<strong>{{ $errors->first('password_confirmation') }}</strong>
									</span> 
								@endif 
								<input name="password_confirmation" type="password" class="form-control input-lg" placeholder="Confirm Password"> 
								<i class="ico-lock2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix{{ $errors->has('phone') ? ' has-error' : '' }}">
								@if ($errors->has('phone')) 
									<span class="help-block"> 
										<strong>{{ $errors->first('phone') }}</strong>
									</span> 
								@endif 
								<input name="phone" type="text" class="form-control input-lg" placeholder="Phone"> 
								<i class="ico-phone7 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix{{ $errors->has('role') ? ' has-error' : '' }}">
								@if ($errors->has('role')) 
									<span class="help-block"> 
										<strong>{{ $errors->first('role') }}</strong>
									</span> 
								@endif
								<select class="form-control input-lg" name="role" id="role">
                                	@foreach($roles as $role)
                                		<option value="{{ $role->id }}">{{ ucfirst($role->name) }}</option>
                                	@endforeach
                                </select>
								<i class="ico-lock2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group invisible" id="university-select-wrapper">
							<div class="form-stack clearfix{{ $errors->has('university') ? ' has-error' : '' }}">
								@if ($errors->has('university')) 
									<span class="help-block"> 
										<strong>{{ $errors->first('university') }}</strong>
									</span> 
								@endif
								<input class="form-control input-lg" type="text" name="university" id="signup-university" placeholder="University">
							</div>
						</div>
						
						<div class="form-group nm">
							<button type="submit" class="btn btn-block btn-success">Sign Up</button>
						</div>
					</div>
				</form>
				<!--/ Login form -->

				<hr>
				<!-- horizontal line -->

				<p class="clearfix nm">
					<span class="text-muted">Already have any account? 
						<a class="semibold" href="{{ url('/login') }}">Sign in to it</a>
					</span>
				</p>
			</div>
		</div>
		<!--/ END row -->
	</section>
	<!--/ END Template Container -->
</section>
<!--/ END Template Main -->
@endsection