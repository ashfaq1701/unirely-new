@extends('layouts.base')

@section('all')
<!-- START Template Main -->
<section id="main" role="main">
	<!-- START Template Container -->
	<section class="container">
		<!-- START row -->
		<div class="row">
			<div class="col-lg-4 col-lg-offset-4">
				<!-- Brand -->
				<div class="text-center" style="margin-bottom: 40px;">
					<span class="template-logo template-logo-inverse"></span>
					<h5 class="semibold text-muted mt-10">Login to your account.</h5>
				</div>
				<!--/ Brand -->

				<hr>
				<!-- horizontal line -->
				
				@if (session('status'))
    				<div class="alert alert-success">
        				{{ session('status') }}
    				</div>
				@endif
				@if (session('warning'))
    				<div class="alert alert-warning">
        				{{ session('warning') }}
    				</div>
				@endif

				<!-- Login form -->
				<form class="panel" role="form" method="POST" action="{{ url('/login') }}">
					{{ csrf_field() }}
					<div class="panel-body">
						<div class="form-group">
							<div class="form-stack has-icon clearfix{{ $errors->has('email') ? ' has-error' : '' }}">
								@if ($errors->has('email')) 
									<span class="help-block"> 
										<strong>{{$errors->first('email') }}</strong>
									</span> 
								@endif 
								<input name="email" type="text" class="form-control input-lg" placeholder="Email"> 
								<i class="ico-user2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix{{ $errors->has('password') ? ' has-error' : '' }}">
								@if ($errors->has('password')) 
									<span class="help-block"> 
										<strong>{{$errors->first('password') }}</strong>
									</span> 
								@endif 
								<input name="password" type="password" class="form-control input-lg" placeholder="Password"> 
								<i class="ico-lock2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-xs-6">
									<div class="checkbox custom-checkbox">
										<input type="checkbox" id="customcheckbox" name="remember"
											value="1"> <label for="customcheckbox">&nbsp;&nbsp;Remember
											me</label>
									</div>
								</div>
								<div class="col-xs-6 text-right">
									<a href="{{ url('/password/reset') }}">Lost password?</a>
								</div>
							</div>
						</div>
						<div class="form-group nm">
							<button type="submit" class="btn btn-block btn-success">Sign In</button>
						</div>
					</div>
				</form>
				<!--/ Login form -->

				<hr>
				<!-- horizontal line -->

				<p class="clearfix nm">
					<span class="text-muted">Don't have any account? 
						<a class="semibold" href="{{ url('/register') }}">Sign up to get started</a>
					</span>
				</p>
			</div>
		</div>
		<!--/ END row -->
	</section>
	<!--/ END Template Container -->
</section>
<!--/ END Template Main -->
@endsection