<!-- START Template Sidebar (Left) -->
<aside class="sidebar sidebar-left sidebar-menu">
	<!-- START Sidebar Content -->
	<section class="content slimscroll">
		<!-- START Template Navigation/Menu -->
		<ul id="nav" class="topmenu">
			<li class="menuheader">MAIN MENU</li>
			@foreach($menu as $key=>$item)
				@if(in_array(Auth::user()->role->name, $item['roles']))
					<li>
						<a href="javascript:void(0);" data-toggle="submenu" data-target="#{{ strtolower($key) }}" data-parent="#nav">
       						<span class="figure"><i class="{{ $item['icon'] }}"></i></span>
        					<span class="text">{{ $key }}</span>
       						<span class="arrow"></span>
    					</a>
    					<!-- START 2nd Level Menu -->
    					<ul id="{{ strtolower($key) }}" class="submenu collapse ">
    						@foreach($item['items'] as $subkey=>$subitem)
    							@if(in_array(Auth::user()->role->name, $subitem['roles']))	
        							<li>
            							<a href="{{ $subitem['url'] }}">
                							<span class="text">{{ $subkey }}</span>
            							</a>
            						</li>
            					@endif
            				@endforeach
        				</ul>
        			</li>
        		@endif
    		@endforeach
    	</ul>
    </section>
</aside>
        