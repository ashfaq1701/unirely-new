<!-- START Left nav -->
<ul class="nav navbar-nav navbar-left">
	@if(!Auth::guest() && (Auth::user()->role->name == 'student'))
		<li class="dropdown custom">
			<a href="#" data-toggle="modal" data-target="#universitySelect" class="dropdown-toggle">
                <span class="meta">
                    <span class="icon">
                    	<i class="ico-plus"></i>
                    </span>
            	</span>
       		</a>
		</li>
	@endif
	@if(!Auth::guest())
		<li class="dropdown-custom">
			<a href="/home" class="dropdown-toggle">
				<span class="icon">
					<i class="ico-home"></i>
				</span>
			</a>
		</li>
		<li class="dropdown-custom">
			<a href="/dashboard" class="dropdown-toggle">
				<span class="icon">
					<i class="ico-dashboard"></i>
				</span>
			</a>
		</li>
	@endif
</ul>
<!--/ END Left nav -->