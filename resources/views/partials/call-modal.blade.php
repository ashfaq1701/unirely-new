<div class="modal fade bs-example-modal-lg minimizableModal" id="callModal" tabindex="-1" role="dialog" aria-labelledby="callLabel">
	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times'></i></button>
        		<button type="button" class="close modalMinimize"><i class='fa fa-minus'></i></button>
        		<h4 class="modal-title" id="callLabel">Call</h4>
      		</div>
      		<div class="modal-body">
      			<div class="container-fluid">
      				<div class="row">
      					<div class="col-md-10 col-md-offset-1 remote-vid-parent">
      						<video height="350" id="remote-vid-container" class="vid" style="display: none"></video>
      						<audio src="" autoplay="autoplay" hidden="hidden" id="remote-audio">
      						</audio>
      						<div id="remote-vid-placeholder">
      							<img id="call-modal-profile-image" class="img-circle" src="/images/28eeada2-8836-4efa-b7cc-624996af7ca3.jpg"><br/>
      							<span id="calling-title">Calling</span> <span id="call-appended">from</span><br/>
      							<span id="caller-name">Md Ashfaq Salehin</span>
      						</div>
      					</div>
      					<div class="local-vid-parent">
      						<video height="100" id="local-vid-container" class="vid"></video>
      					</div>
      					<div class="col-md-12" id="timer-container">
      						
      					</div>
      					<div class="col-md-12" id="control-container-parent">
      						<div id="control-container">
      							<span class="fa-stack fa-2x">
      								<span id="call-receive" style="color: green" class="fa fa-phone fa-2x"></span>
      							</span>
      							<span class="fa-stack fa-2x">
      								<span id="call-end" style="color: red" class="fa fa-phone fa-2x"></span>
      							</span>
      							<span class="fa-stack fa-2x" id="video-on-off">
									<i class="fa fa-video-camera fa-stack-1x"></i>
									<i class="fa fa-ban fa-stack-2x" id="video-off"></i>
      							</span>
      						</div>
      					</div>
      				</div>
      			</div>
      		</div>
      	</div>
    </div>
</div>
<div class="minmaxCon"></div>  