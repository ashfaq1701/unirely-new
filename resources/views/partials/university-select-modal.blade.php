<div class="modal fade bs-example-modal-lg" id="universitySelect" tabindex="-1" role="dialog" aria-labelledby="universitySelectLabel">
	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        		<h4 class="modal-title" id="universitySelectLabel">Add Universities</h4>
      		</div>
      		<div class="modal-body">
      			<div class="row">
      				<div class="col-md-3 col-md-offset-9 price-container">
      					Total Price: <span id="total-price">0.00</span> USD
      				</div>
      				<div class="col-md-3 bottom-gap">
      					<label class="control-label right-align">Search Universities</label>
      				</div>
      				<div class="col-md-8 bottom-gap">
      					<input type="text" class="form-control" id="search-universities"/>
      				</div>
      				
      				<div class="universities-container">
      					@foreach($universities as $university)
      						<div class="col-md-6 text-center single-university">
      							<label><input type="checkbox" class="university-input" value="{{ $university->id }}" id="university-{{ $university->id }}">
      								<span class="single-university-name">{{ $university->name }}</span>
      								<div class="row invisible price-wrapper">
      									@for($i = 0; $i < $packages->count(); $i++)
      										<div class="col-md-4">
      											<input type="radio" name="university-plan-{{ $university->id }}" class="university-plan" id="university-plan-{{ $university->id }}-{{ $packages->get($i)->id }}" value="{{ $packages->get($i)->payment }}"> 
      											<br/>
      											<span class="plan-title">{{ $packages->get($i)->name }}</span><br/>
      											<span class="plan-validity">{{ $packages->get($i)->validity }} Days</span><br/>
      											<span class="plan-price">{{ number_format($packages->get($i)->payment) }} USD</span>
      										</div>
      									@endfor
      								</div>
      							</label>
      						</div>
      					@endforeach
      				</div>
      				<div class="col-md-12 bottom-gap top-gap">
      					<p class="center-align">
      						<strong>Could Not Find Your Desired University? Request To Add...</strong>
      					</p>
      				</div>
      				<div class="col-md-12">
      					<form class="form-inline align-center" role="form">
      						<div class="form-group">
    							<label for="request-university-name">University Name:</label>
    							<input type="text" class="form-control" id="request-university-name" name="name">
  							</div>
  							<a id="request-university" class="btn btn-default">Request To Add</a>
      					</form>
      				</div>
      			</div>
      			<form action="/application/create" method="POST" id="university_submit">
      				<input type="hidden" name="_token" value="{{ csrf_token() }}">
      			</form>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        		<button type="button" class="btn btn-primary" id="university-modal-submit">Subscribe</button>
      		</div>
    	</div>
	</div>
</div>