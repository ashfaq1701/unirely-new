@extends('layouts.content-with-sidebar')

@section('page-title')
	Create University
@endsection

@section('main-content')
<form method="POST" action="universities/{{ $university->id }}">
	{{ csrf_field() }}
	<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
		@if ($errors->has('name')) 
			<span class="help-block col-lg-offset-2 col-lg-10"> 
				<strong>{{$errors->first('name') }}</strong>
			</span> 
		@endif 
		<label class="col-md-3 control-label">Name:</label>
		<div class="col-lg-8">
			<input class="form-control" name="name" type="text" value="{{ $university->name }}">
		</div>
	</div>
	<button type="submit" class="btn btn-primary">Edit University</button>
</form>
@endsection