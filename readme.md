# UniRely Website

Unirely website developed using Laravel 5.2

This website has three process which needs to be run (or workers).

1. HTTP server (runs through php artisan serve)
2. WS server (runs through php artisan socket:run)
3. Queue Worker (runs through php artisan queue:listen database)