<?php $__env->startSection('page-title'); ?>
	Messages
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-css'); ?>
	<link rel="stylesheet" href="<?php echo e(asset('assets/css/chat.css')); ?>">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<!-- message list-->
<!-- START Table layout -->
<div class="col-lg-4 valign-top panel panel-minimal">
	<!-- panel heading -->
	<div class="panel-heading">
		<!-- panel toolbar -->
		<div class="panel-toolbar">
			<div class="input-group">
				<div class="has-icon">
					<input type="text" class="form-control" id="search-contacts" placeholder="Search Contacts..."> 
					<i class="ico-search form-control-icon"></i>
				</div>
			</div>
		</div>
	</div>
	<!--/ panel heading -->

	<hr class="mt10 mb10">
	<!-- horizontal line -->
	<!-- panel body -->
	<div class="panel-body np">
		<!-- message list -->
		<div class="media-list chat-contact-list">
			<?php echo $__env->make('chats.partials.messaging-channel-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		</div>
		<!-- message list -->
	</div>
	<!--/ panel body -->
</div>
<!--/ message list -->

<!-- message content -->
<div class="col-lg-8 valign-top panel panel-primary right-section">
	<!-- panel heading -->
	<div class="panel-heading chat-container">
		<?php echo $__env->make('chats.partials.content-area-head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
	<!--/ panel heading -->

	<!-- panel body -->
	<div class="panel-body np chat-container" id="chats-content">
		<?php echo $__env->make('chats.partials.content-area-chat', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
	<!-- panel body -->

	<!-- panel footer -->
	<div class="panel-footer chat-container" id="chats-footer">
		<?php echo $__env->make('chats.partials.content-area-footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	</div>
	<!-- panel footer -->
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.content-without-sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>