<?php $__env->startSection('panel-heading'); ?>
	View Application Assignment
<?php $__env->stopSection(); ?>

<?php $__env->startSection('panel-body'); ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h2>Application To <?php echo e($assignment->application->university->name); ?></h2>
				<p>Name of the applicant: <?php echo e($assignment->application->user->name); ?></p>
				<p>Selected Plan: <?php echo e($assignment->application->package->name); ?></p>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('panel-footer'); ?>
	<div class="panel-footer appended-content">
		<button class="btn btn-default" onclick="acceptAssignment(<?php echo e($assignment->id); ?>)">Accept</button>
		<button class="btn btn-default" onclick="rejectAssignment(<?php echo e($assignment->id); ?>)">Reject</button>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('chats.partials.appended-panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>