<div class="col-md-5">
	Mentor Name: <?php echo e($user->name); ?><br>
	University: <?php echo e($user->university->name); ?>

</div>
<div class="col-md-1 col-md-offset-1">
<?php if($channel->otherUser()->is_online == 1): ?>
	<span class="ico-phone2" onclick="doCall(<?php echo e(empty($channel->otherUser()->peer_id) ? 0 : "'".$channel->otherUser()->peer_id."'"); ?>)"></span>
<?php endif; ?>
</div>
<div class="col-md-5">
	<?php echo e($plan); ?> Plan:
	<?php if($difference >= 0): ?> 
	    <?php echo e($difference); ?> Days Left
	<?php else: ?>
	    Expired <?php echo e(abs($difference)); ?> Days Ago<br/>
	    <a class="renew-links" href="/renew/<?php echo e($channel->id); ?>?mentor=same">Renew Same Mentor</a>&nbsp
	    <a class="renew-links" href="/renew/<?php echo e($channel->id); ?>?mentor=new">Renew</a>
	<?php endif; ?>
</div>