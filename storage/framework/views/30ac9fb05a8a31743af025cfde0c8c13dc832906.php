<div class="col-md-6">
	Student Name: <?php echo e($user->name); ?><br>
</div>
<div class="col-md-4 col-md-offset-2">
	<?php echo e($plan); ?> Plan:
	<?php if($difference >= 0): ?> 
	    <?php echo e($difference); ?> Days Left
	<?php else: ?>
	    Expired <?php echo e(abs($difference)); ?> Days Ago
	<?php endif; ?>
</div>