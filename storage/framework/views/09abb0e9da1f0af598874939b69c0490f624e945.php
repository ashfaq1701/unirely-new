<a href="#" class="media" onclick="channelSelected('<?php echo e($channel->id); ?>')"> 
	<span class="pull-left">
		<?php if(empty($channel->otherUser()->profile_image)): ?>
			<img src="<?php echo e(config('constants.dummy_profile_image')); ?>" class="media-object img-circle" alt="" width="50px" height="50px;">
		<?php else: ?>
			<img src="<?php echo e('/images/'.$channel->otherUser()->profile_image); ?>" class="media-object img-circle" alt="" width="50px" height="50px;">
		<?php endif; ?>
	</span> 
	<span class="media-body">
		<span id="<?php echo e($channel->id); ?>-online-status" class="hasnotification hasnotification-<?php echo e(($channel->otherUser()->is_online == 1) ? 'success' : 'default'); ?> mr5"></span>
		<span class="media-heading contact-name"><?php echo e($channel->otherUser()->name); ?></span> 
		<span class="media-text ellipsis nm">
			<?php if(!empty($channel->lastMessage())): ?>
				<?php echo e(isset($channel->lastMessage()->text) ? $channel->lastMessage()->text : 'Attachment'); ?>

			<?php else: ?>
				No message exchange found
			<?php endif; ?>
		</span>
		<?php if(!empty($channel->lastMessage())): ?>
			<?php if(($channel->lastMessage()->type == 'image') && ($channel->lastMessage()->type == 'file')): ?>
				<span class="media-meta">
					<i class="ico-attachment"></i>
				</span>
			<?php endif; ?>
		<?php endif; ?>
		<span class="media-meta pull-right"><?php echo e(carbonToReadableString($channel->otherUser()->last_seen_at)); ?></span>
	</span>
</a>