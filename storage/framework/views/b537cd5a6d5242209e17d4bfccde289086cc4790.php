<?php $__env->startSection('page-title'); ?>
	Landing Page
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<section id="main" role="main">
	<!-- START Template Container -->
	<section class="container">
		<!-- START row -->
		<div class="row">
			<div class="col-lg-4 col-lg-offset-4">
				Your applications landing page.
			</div>
		</div>
	</section>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.content-without-sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>