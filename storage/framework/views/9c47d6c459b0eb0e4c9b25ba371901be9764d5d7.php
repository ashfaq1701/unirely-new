<?php $__env->startSection('page-title'); ?>
	Profile
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
<div class="container">
	<div class="row">
		<?php if((Auth::user()->role->name == 'admin') && ($user->role->name != 'admin')): ?>
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-2 col-md-offset-10">
						<a href="/user/chat-list/<?php echo e($user->id); ?>" class="btn btn-success">View Chats</a>
					</div>
				</div>
			</div>
		<?php endif; ?>
		<!-- left column -->
		<div class="col-md-3">
			<div class="text-center">
				<?php if(empty($user->profile_image)): ?>
					<img src="<?php echo e(config('constants.dummy_profile_image')); ?>" class="avatar img-circle" width="125px" height="125px" alt="avatar">
				<?php else: ?>
					<img src="<?php echo e('/images/'.$user->profile_image); ?>" class="avatar img-circle" width="125px" height="125px" alt="avatar">
				<?php endif; ?>
				<h6>Upload a different photo...</h6>
          		<input type="file" class="form-control" name="profile-picture" id="profile-picture">
        	</div>
		</div>
      
		<!-- edit form column -->
		<div class="col-md-9 personal-info">
			<?php if(session('status')): ?>
    			<div class="alert alert-success">
        			<?php echo e(session('status')); ?>

    			</div>
			<?php endif; ?>
			<?php if(session('warning')): ?>
    			<div class="alert alert-warning">
        			<?php echo e(session('warning')); ?>

    			</div>
			<?php endif; ?>
			<h3>Personal info</h3>
        
			<form class="form-horizontal" role="form" 
			<?php if((Auth::user()->id == $user->id) || (Auth::user()->role->name == 'admin')): ?>
				action="/profile/save<?php echo e((Auth::user()->role->name == 'admin') ? '/'.$user->id : ''); ?>" method="POST" 
			<?php endif; ?>
			id="profile-form">
				<?php echo e(csrf_field()); ?>

				<input type="hidden" id="profile-image" name="profile-image" val=""/>
				<div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
					<?php if($errors->has('name')): ?> 
						<span class="help-block col-lg-offset-2 col-lg-10"> 
							<strong><?php echo e($errors->first('name')); ?></strong>
						</span> 
					<?php endif; ?> 
					<label class="col-lg-3 control-label">Name:</label>
					<div class="col-lg-8">
						<input class="form-control" name="name" type="text" value="<?php echo e(isset($user->name) ? $user->name : ''); ?>">
					</div>
				</div>
				<div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
					<?php if($errors->has('email')): ?> 
						<span class="help-block col-lg-offset-2 col-lg-10"> 
							<strong><?php echo e($errors->first('email')); ?></strong>
						</span> 
					<?php endif; ?> 
					<label class="col-lg-3 control-label">Email:</label>
					<div class="col-lg-8">
						<input class="form-control" name="email" type="text" value="<?php echo e(isset($user->email) ? $user->email : ''); ?>">
					</div>
				</div>
				<div class="form-group<?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
					<?php if($errors->has('phone')): ?> 
						<span class="help-block col-lg-offset-2 col-lg-10"> 
							<strong><?php echo e($errors->first('phone')); ?></strong>
						</span> 
					<?php endif; ?> 
					<label class="col-md-3 control-label">Phone:</label>
					<div class="col-md-8">
						<input class="form-control" name="phone" type="text" value="<?php echo e(isset($user->phone) ? $user->phone : ''); ?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">City:</label>
					<div class="col-md-8">
						<input class="form-control" name="city" type="text" value="<?php echo e(isset($user->userMeta->city) ? $user->userMeta->city : ''); ?>">
					</div>
				</div>
				<?php if($user->role->name == 'mentor'): ?>
					<div class="form-group">
						<label class="col-md-3 control-label">University:</label>
						<div class="col-md-8">
							<select class="form-control" name="university">
								<?php foreach($universities as $university): ?>
									<option value="<?php echo e($university->id); ?>" <?php echo e((($user->university->id) == $university->id) ? 'selected' : ''); ?>><?php echo e($university->name); ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				<?php endif; ?>
				<?php if($user->role->name == 'student'): ?>
          			<div class="form-group">
						<label class="col-md-3 control-label">Percentage or Grade (In 12th):</label>
						<div class="col-md-8">
							<input class="form-control" type="text" name="percentage" value="<?php echo e(isset($user->userMeta->percentage) ? $user->userMeta->percentage : ''); ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Courses Interested</label>
						<div class="col-md-8">
							<input class="form-control" type="text" id="courses-interested" name="courses-interested">
						</div>
					</div>
				<?php endif; ?>
				<script type="text/javascript">
					var courses = JSON.parse('<?php echo json_encode($user->courses); ?>');
					var userId = <?php echo e(Auth::user()->id); ?>;
				</script>
				<?php if((Auth::user()->id == $user->id) || Auth::user()->role->name == 'admin'): ?>
					<div class="form-group">
						<label class="col-md-3 control-label"></label>
						<div class="col-md-8">
							<button type="submit" class="btn btn-primary" id="profile-save">Save</button>
							<span></span>
							<input type="reset" class="btn btn-default" value="Cancel">
            			</div>
          			</div>
          		<?php endif; ?>
			</form>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
	<script type="text/javascript" src="<?php echo e(asset('assets/js/profile.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.content-without-sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>