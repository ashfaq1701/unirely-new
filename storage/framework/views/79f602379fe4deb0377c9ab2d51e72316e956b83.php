<?php $__env->startSection('all'); ?>
<!-- START Template Main -->
<section id="main" role="main">
	<!-- START Template Container -->
	<section class="container">
		<!-- START row -->
		<div class="row">
			<div class="col-lg-4 col-lg-offset-4">
				<!-- Brand -->
				<div class="text-center" style="margin-bottom: 40px;">
					<span class="template-logo template-logo-inverse"></span>
					<h5 class="semibold text-muted mt-10">Register for a new account.</h5>
				</div>
				<!--/ Brand -->

				<hr>
				<!-- horizontal line -->

				<!-- Login form -->
				<form class="panel" role="form" method="POST" action="<?php echo e(url('/register')); ?>">
					<?php echo e(csrf_field()); ?>

					<div class="panel-body">
						<div class="form-group">
							<div class="form-stack has-icon clearfix<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
								<?php if($errors->has('name')): ?> 
									<span class="help-block"> 
										<strong><?php echo e($errors->first('name')); ?></strong>
									</span> 
								<?php endif; ?> 
								<input name="name" type="text" class="form-control input-lg" placeholder="Name"> 
								<i class="ico-user2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
								<?php if($errors->has('email')): ?> 
									<span class="help-block"> 
										<strong><?php echo e($errors->first('email')); ?></strong>
									</span> 
								<?php endif; ?> 
								<input name="email" type="text" class="form-control input-lg" placeholder="Email"> 
								<i class="ico-user2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
								<?php if($errors->has('password')): ?> 
									<span class="help-block"> 
										<strong><?php echo e($errors->first('password')); ?></strong>
									</span> 
								<?php endif; ?> 
								<input name="password" type="password" class="form-control input-lg" placeholder="Password"> 
								<i class="ico-lock2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix<?php echo e($errors->has('password_confirmation') ? ' has-error' : ''); ?>">
								<?php if($errors->has('password_confirmation')): ?> 
									<span class="help-block"> 
										<strong><?php echo e($errors->first('password_confirmation')); ?></strong>
									</span> 
								<?php endif; ?> 
								<input name="password_confirmation" type="password" class="form-control input-lg" placeholder="Confirm Password"> 
								<i class="ico-lock2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix<?php echo e($errors->has('phone') ? ' has-error' : ''); ?>">
								<?php if($errors->has('phone')): ?> 
									<span class="help-block"> 
										<strong><?php echo e($errors->first('phone')); ?></strong>
									</span> 
								<?php endif; ?> 
								<input name="phone" type="text" class="form-control input-lg" placeholder="Phone"> 
								<i class="ico-phone7 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group">
							<div class="form-stack has-icon clearfix<?php echo e($errors->has('role') ? ' has-error' : ''); ?>">
								<?php if($errors->has('role')): ?> 
									<span class="help-block"> 
										<strong><?php echo e($errors->first('role')); ?></strong>
									</span> 
								<?php endif; ?>
								<select class="form-control input-lg" name="role" id="role">
                                	<?php foreach($roles as $role): ?>
                                		<option value="<?php echo e($role->id); ?>"><?php echo e(ucfirst($role->name)); ?></option>
                                	<?php endforeach; ?>
                                </select>
								<i class="ico-lock2 form-control-icon"></i>
							</div>
						</div>
						<div class="form-group invisible" id="university-select-wrapper">
							<div class="form-stack clearfix<?php echo e($errors->has('university') ? ' has-error' : ''); ?>">
								<?php if($errors->has('university')): ?> 
									<span class="help-block"> 
										<strong><?php echo e($errors->first('university')); ?></strong>
									</span> 
								<?php endif; ?>
								<input class="form-control input-lg" type="text" name="university" id="signup-university" placeholder="University">
							</div>
						</div>
						
						<div class="form-group nm">
							<button type="submit" class="btn btn-block btn-success">Sign Up</button>
						</div>
					</div>
				</form>
				<!--/ Login form -->

				<hr>
				<!-- horizontal line -->

				<p class="clearfix nm">
					<span class="text-muted">Already have any account? 
						<a class="semibold" href="<?php echo e(url('/login')); ?>">Sign in to it</a>
					</span>
				</p>
			</div>
		</div>
		<!--/ END row -->
	</section>
	<!--/ END Template Container -->
</section>
<!--/ END Template Main -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>