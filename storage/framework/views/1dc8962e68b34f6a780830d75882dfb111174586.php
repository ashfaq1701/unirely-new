<div class="modal fade bs-example-modal-lg minimizableModal" id="callModal" tabindex="-1" role="dialog" aria-labelledby="callLabel">
	<div class="modal-dialog modal-lg" role="document">
    	<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class='fa fa-times'></i></button>
        		<button type="button" class="close modalMinimize"><i class='fa fa-minus'></i></button>
        		<h4 class="modal-title" id="callLabel">Call</h4>
      		</div>
      		<div class="modal-body">
      		</div>
      	</div>
    </div>
</div>
<div class="minmaxCon"></div>  