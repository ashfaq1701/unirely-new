<!-- START Template Sidebar (Left) -->
<aside class="sidebar sidebar-left sidebar-menu">
	<!-- START Sidebar Content -->
	<section class="content slimscroll">
		<!-- START Template Navigation/Menu -->
		<ul id="nav" class="topmenu">
			<li class="menuheader">MAIN MENU</li>
			<?php foreach($menu as $key=>$item): ?>
				<?php if(in_array(Auth::user()->role->name, $item['roles'])): ?>
					<li>
						<a href="javascript:void(0);" data-toggle="submenu" data-target="#<?php echo e(strtolower($key)); ?>" data-parent="#nav">
       						<span class="figure"><i class="<?php echo e($item['icon']); ?>"></i></span>
        					<span class="text"><?php echo e($key); ?></span>
       						<span class="arrow"></span>
    					</a>
    					<!-- START 2nd Level Menu -->
    					<ul id="<?php echo e(strtolower($key)); ?>" class="submenu collapse ">
    						<?php foreach($item['items'] as $subkey=>$subitem): ?>
    							<?php if(in_array(Auth::user()->role->name, $subitem['roles'])): ?>	
        							<li>
            							<a href="<?php echo e($subitem['url']); ?>">
                							<span class="text"><?php echo e($subkey); ?></span>
            							</a>
            						</li>
            					<?php endif; ?>
            				<?php endforeach; ?>
        				</ul>
        			</li>
        		<?php endif; ?>
    		<?php endforeach; ?>
    	</ul>
    </section>
</aside>
        