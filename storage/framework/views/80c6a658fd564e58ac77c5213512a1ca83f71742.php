<?php $__env->startSection('page-title'); ?>
	All Mentors
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
	<table class="table table-striped datatable">
		<thead>
			<tr>
				<th>Name</th>
				<th>University</th>
				<th>Email</th>
				<th>Status</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($mentors as $mentor): ?>
				<tr>
					<td><?php echo e($mentor->name); ?></td>
					<td><?php echo e($mentor->university->name); ?></td>
					<td><?php echo e($mentor->email); ?></td>
					<td><?php echo e(($mentor->active == 1) ? 'Active' : 'Inactive'); ?></td>
					<td>
						<a href="/profile/<?php echo e($mentor->id); ?>">
							<span class="ico-profile"></span>
						</a>
						<?php if($mentor->active == 0): ?>
							<a href="#" onclick="approveMentor(<?php echo e($mentor->id); ?>)">
								<span class="ico-ok-circle"></span>	
							</a>
						<?php else: ?>
							<a href="#" onclick="banMentor(<?php echo e($mentor->id); ?>)">
								<span class="ico-ban-circle"></span>	
							</a>
						<?php endif; ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.content-with-sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>