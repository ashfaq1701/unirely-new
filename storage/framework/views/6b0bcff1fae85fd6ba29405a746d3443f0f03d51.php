<?php $__env->startSection('page-title'); ?>
	Chat List
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
	<p><strong>Email</strong> : <?php echo e($user->email); ?></p>
	<p><strong>Name</strong> : <?php echo e($user->name); ?></p>
	<p><strong>Role</strong> : <?php echo e(ucfirst($user->role->name)); ?></p>
	<table class="table table-striped datatable">
		<thead>
			<tr>
				<th><?php echo e($user->role->name == 'mentor' ? 'Student' : 'Mentor'); ?> Email</th>
				<th><?php echo e($user->role->name == 'mentor' ? 'Student' : 'Mentor'); ?> Name</th>
				<th>Expiration Date</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($channels as $channel): ?>
				<tr>
					<td><?php echo e($channel->otherThanGivenUser($user)->email); ?></td>
					<td><?php echo e($channel->otherThanGivenUser($user)->name); ?></td>
					<td><?php echo e(!empty($channel->subscription) ? $channel->subscription->valid_till : ' - '); ?></td>
					<td>
						<a href="#" onclick="viewChatsAdmin(<?php echo e($channel->id); ?>)">
							<span class="ico-bubbles10"></span>
						</a>
					</td>
				</tr>	
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php echo $__env->make('admin.partials.channel-messages-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.content-with-sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>