<h2>Unirely : New Message Received</h2>

<p>
	You received a new email From <?php echo e($messageObj->user->name); ?>

	<?php if($messageObj->user->role->name == 'mentor'): ?>
		<?php echo e(' - '. $messageObj->user->university->name); ?>

	<?php endif; ?>
</p>

<?php if($messageObj->type == 'text'): ?>
<p>
	<strong>Text : </strong>
	<?php echo e($messageObj->text); ?>

</p>
<?php elseif($messageObj->type == 'image'): ?>
<p>
	<strong>URL : </strong>
	<a href="<?php echo e($messageObj->file_path); ?>"><?php echo e(basename($messageObj->file_path)); ?></a>
</p>
<img style="height: 400px; width: 400px;" src="<?php echo e($messageObj->file_path); ?>"/>

<?php elseif($messageObj->type == 'file'): ?>
<p>
	<strong>URL : </strong>
	<a href="<?php echo e($messageObj->file_path); ?>"><?php echo e(basename($messageObj->file_path)); ?></a>
</p>
<?php endif; ?>
