<div class="panel-toolbar-wrapper">
	<div class="panel-toolbar">
		<div class="input-group" style="width: 100%;">
			<span class="input-group-btn">
				<label class="btn btn-primary">
    				<i class="ico-attachment"></i>Attach
    				<input type="file" id="chat_file_input" style="display: none;">
				</label>
			</span>
			<input type="text" class="form-control chat-input-text__field" placeholder="Type your message"> 
			<span class="input-group-btn">
				<button class="btn btn-primary" type="button" id="send-message">
					<i class="ico-paper-plane"></i> <span class="semibold">Send</span>
				</button>
			</span>
		</div>
		<p id="typing-indicator"><span id="username-typing"></span> is typing...</p>
	</div>
</div>