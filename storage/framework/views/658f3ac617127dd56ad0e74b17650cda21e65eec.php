<div class="col-md-5">
    Member Name: <?php echo e($user->name); ?><br>
    Role: <?php echo e(ucfirst($user->role->name)); ?> 
    <?php if($user->role->name == 'mentor'): ?>
    	(<?php echo e($user->university->name); ?>)
    <?php endif; ?>
</div>
<div class="col-md-1">
<?php if($channel->otherUser()->is_online == 1): ?>
	<span class="ico-phone2" onclick="doCall(<?php echo e(empty($channel->otherUser()->peer_id) ? 0 : "'".$channel->otherUser()->peer_id."'"); ?>)"></span>
<?php endif; ?>
</div>