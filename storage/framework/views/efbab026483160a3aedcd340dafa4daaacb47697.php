<a href="#" class="media" onclick="channelSelected('<?php echo e($adminChannel->id); ?>')"> 
	<span class="pull-left">
		<?php if(empty($adminChannel->otherUser()->profile_image)): ?>
			<img src="<?php echo e(config('constants.dummy_profile_image')); ?>" class="media-object img-circle" alt="" width="50px" height="50px;">
		<?php else: ?>
			<img src="<?php echo e('/images/'.$adminChannel->otherUser()->profile_image); ?>" class="media-object img-circle" alt="" width="50px" height="50px;">
		<?php endif; ?>
	</span> 
	<span class="media-body">
		<span id="<?php echo e($adminChannel->id); ?>-online-status" class="hasnotification hasnotification-<?php echo e(($adminChannel->otherUser()->is_online == 1) ? 'success' : 'default'); ?> mr5"></span>
		<span class="media-heading contact-name">Admin</span> 
		<span <?php echo !empty($adminChannel->unreadMessagesCount()) ? 'class="label label-danger" ' : ''; ?>id="unread-<?php echo e($adminChannel->id); ?>"><?php echo e($adminChannel->unreadMessagesCount()); ?></span>
		<span class="media-text ellipsis nm">
			<?php if(!empty($adminChannel->lastMessage())): ?>
				<?php echo e(isset($adminChannel->lastMessage()->text) ? $adminChannel->lastMessage()->text : 'Attachment'); ?>

			<?php else: ?>
				No message exchange found
			<?php endif; ?>
		</span>
		<?php if(!empty($adminChannel->lastMessage())): ?>
			<?php if(($adminChannel->lastMessage()->type == 'file') || ($adminChannel->lastMessage()->type == 'image')): ?>
				<span class="media-meta">
					<i class="ico-attachment"></i>
				</span>
			<?php endif; ?>
		<?php endif; ?>
		<span class="media-meta pull-right"><?php echo e(carbonToReadableString($adminChannel->otherUser()->last_seen_at)); ?></span>
	</span>
</a>