<?php if(Auth::user()->role->name == 'admin'): ?>
	<?php echo $__env->make('chats.partials.admin-messaging-channel-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php elseif(Auth::user()->role->name == 'mentor'): ?>
	<?php echo $__env->make('chats.partials.mentor-messaging-channel-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php elseif(Auth::user()->role->name == 'student'): ?>
	<?php echo $__env->make('chats.partials.student-messaging-channel-list', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>