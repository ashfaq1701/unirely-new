<!-- START Template Main -->
<section id="main" role="main">
	<!-- START Template Container -->
    	<section class="container-fluid">
    		<!-- Page Header -->
			<div class="page-header page-header-block">
				<div class="page-header-section">
					<h4 class="title semibold">
						<?php echo $__env->yieldContent('page-title'); ?>
					</h4>
				</div>
			</div>
			<!-- START Table layout -->
			<div class="table-layout nm">
				<?php echo $__env->yieldContent('main-content'); ?>
        	</div>
		</section>
		<a href="#" class="totop animation" data-toggle="waypoints totop" data-marker="#main" data-showanim="bounceIn" data-hideanim="bounceOut" data-offset="-50%"><i class="ico-angle-up"></i></a>
		<!--/ END To Top Scroller -->
</section>
<!--/ END Template Main -->