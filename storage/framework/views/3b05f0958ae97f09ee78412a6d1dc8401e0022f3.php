<p><strong>You will be charged for the following orders,</strong></p>
<table class="table table-bordered">
	<thead>
		<tr>
			<th>University Name</th>
			<th>Plan Name</th>
			<th>Price</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($details as $detail): ?>
			<tr>
				<td><?php echo e($detail['university_name']); ?></td>
				<td><?php echo e($detail['package_name']); ?></td>
				<td><?php echo e($detail['price']); ?></td>
			</tr>
		<?php endforeach; ?>
		<tr>
			<td><strong>Total</strong></td>
			<td></td>
			<td><?php echo e($totalPrice); ?></td>
		</tr>
	</tbody>
</table>