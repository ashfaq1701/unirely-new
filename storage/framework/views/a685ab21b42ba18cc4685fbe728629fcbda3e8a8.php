<?php $__env->startSection('all'); ?>
<!-- START Template Header -->
<header id="header" class="navbar navbar-fixed-top">
	<!-- START navbar header -->
	<div class="navbar-header">
		<!-- Brand -->
		<a class="navbar-brand" href="index.html"> <span class="template-logo"></span>
		</a>
		<!--/ Brand -->
	</div>
	<!--/ END navbar header -->

	<!-- START Toolbar -->
	<div class="navbar-toolbar clearfix">
		<?php echo $__env->make('partials.left-nav-bar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		<!-- START Right nav -->
		<ul class="nav navbar-nav navbar-right">
			<!-- Authentication Links -->
			<?php if(Auth::guest()): ?>
				<li class="dropdown profile">
					<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> 
						<span class="meta">
							<span class="text hidden-xs hidden-sm">Login / Register</span> 
							<span class="arrow"></span>
						</span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a href="<?php echo e(url('/login')); ?>">
								<span class="icon">
									<i class="ico-exit"></i>
								</span> 
								Login
							</a>
						</li>
						<li>
							<a href="<?php echo e(url('/register')); ?>">
								<span class="icon">
									<i class="ico-exit"></i>
								</span> 
								Register
							</a>
						</li>
					</ul>
				</li> 
			<?php else: ?>
				<!-- Profile dropdown -->
				<li class="dropdown profile">
					<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"> 
						<span class="meta">
							<span class="avatar">
								<?php if(empty(Auth::user()->profile_image)): ?>
									<img src="<?php echo e(config('constants.dummy_profile_image')); ?>" class="img-circle" alt="" />
								<?php else: ?>
									<img src="<?php echo e('/images/'.Auth::user()->profile_image); ?>" class="img-circle" alt="" />
								<?php endif; ?>
							</span> 
							<span class="text hidden-xs hidden-sm"><?php echo e(Auth::user()->name); ?></span> 
							<span class="arrow"></span>
						</span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<li class="pa15">
							<h5 class="semibold hidden-xs hidden-sm">
								60% <br /> 
								<small class="text-muted">Profile complete</small>
							</h5>
							<h5 class="semibold hidden-md hidden-lg">
								<?php echo e(Auth::user()->name); ?><br /> 
								<small class="text-muted">60% Profile complete</small>
							</h5>
							<div class="progress progress-xs nm mt10">
								<div class="progress-bar progress-bar-warning" style="width: 60%">
									<span class="sr-only">60% Complete</span>
								</div>
							</div>
						</li>
						<li class="divider"></li>
						<?php foreach($dropdownMenu as $name=>$item): ?>
							<?php if(in_array(Auth::user()->role->name, $item['roles'])): ?>
								<li>
									<a href="/<?php echo e($item['url']); ?>">
										<span class="icon">
											<i class="<?php echo e($item['icon']); ?>"></i>
										</span>
										<?php echo e($name); ?>

									</a>
								</li>
							<?php endif; ?>
						<?php endforeach; ?>
						<li class="divider"></li>
						<li>
							<a href="<?php echo e(url('/logout')); ?>">
								<span class="icon">
									<i class="ico-exit"></i>
								</span> 
								Sign Out
							</a>
						</li>
					</ul>
				</li>
				<!--/ Profile dropdown -->
			<?php endif; ?>
		</ul>
		<!--/ END Right nav -->
	</div>
	<!--/ END Toolbar / link -->
</header>
<!--/ END Template Header -->

<?php echo $__env->yieldContent('content'); ?>

<?php if(!Auth::guest()): ?>
	<?php echo $__env->make('partials.call-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>

<?php if(!Auth::guest() && (Auth::user()->role->name == 'student')): ?>
	<?php echo $__env->make('partials.university-select-modal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php endif; ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
	<?php if(!Auth::guest()): ?>
		<script type="text/javascript" src="<?php echo e(asset('assets/js/chat.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('assets/js/call-ui.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('assets/js/peer-custom.js')); ?>"></script>
	<?php endif; ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.base', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>