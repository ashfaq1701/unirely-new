<?php $__env->startSection('page-title'); ?>
	Checkout
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
	<?php echo $__env->make('payments.partials.details', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php echo $__env->make('payments.partials.payment-form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('custom-scripts'); ?>
	<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<script type="text/javascript">
  		Stripe.setPublishableKey("<?php echo e(config('services.stripe.key')); ?>");
	</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.content-without-sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>