<?php echo $__env->make('chats.partials.admin-channel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

<hr />
<p class="menu-divider">
	<span>PENDING</span>
</p>
<hr />
<?php for($i = 0; $i < $assignments->count(); $i++): ?>
	<a href="#" class="media" id="assignment-container-<?php echo e($assignments->get($i)->id); ?>" onclick="viewApplication(<?php echo e($assignments->get($i)->id); ?>)"> 
		<span class="pull-left">
			<?php if(empty($assignments->get($i)->application->user->profile_image)): ?>
				<img src="<?php echo e(config('constants.dummy_profile_image')); ?>" class="media-object img-circle" alt="" width="50px" height="50px;">
			<?php else: ?>
				<img src="<?php echo e('/images/'.$assignments->get($i)->application->user->profile_image); ?>" class="media-object img-circle" alt="" width="50px" height="50px;">
			<?php endif; ?>
		</span> 
		<span class="media-body">
			<span class="hasnotification hasnotification-default mr5"></span>
			<span class="media-heading contact-name"><?php echo e($assignments->get($i)->application->user->name); ?></span> 
			<span class="media-text ellipsis nm">
				Assignment needs your approval
			</span>
		</span>
	</a>
<?php endfor; ?>

<hr />
<p class="menu-divider">
	<span>ACTIVE</span>
</p>
<hr id="active-head"/>
<?php for($i = 0; $i < $activeChannels->count(); $i++): ?>
	<a href="#" class="media" onclick="channelSelected('<?php echo e($activeChannels->get($i)->id); ?>')"> 
		<span class="pull-left">
			<?php if(empty($activeChannels->get($i)->otherUser()->profile_image)): ?>
				<img src="<?php echo e(config('constants.dummy_profile_image')); ?>" class="media-object img-circle" alt="" width="50px" height="50px;">
			<?php else: ?>
				<img src="<?php echo e('/images/'.$activeChannels->get($i)->otherUser()->profile_image); ?>" class="media-object img-circle" alt="" width="50px" height="50px;">
			<?php endif; ?>
		</span> 
		<span class="media-body">
			<span id="<?php echo e($activeChannels->get($i)->id); ?>-online-status" class="hasnotification hasnotification-<?php echo e(($activeChannels->get($i)->otherUser()->is_online == 1) ? 'success' : 'default'); ?> mr5"></span>
			<span class="media-heading contact-name"><?php echo e($activeChannels->get($i)->otherUser()->name); ?></span> 
			<span class="media-text ellipsis nm">
				<?php if(!empty($activeChannels->get($i)->lastMessage())): ?>
					<?php echo e(isset($activeChannels->get($i)->lastMessage()->text) ? $activeChannels->get($i)->lastMessage()->text : 'Attachment'); ?>

				<?php else: ?>
					No message exchange found
				<?php endif; ?>
			</span>
			<?php if(!empty($activeChannels->get($i)->lastMessage())): ?>
				<?php if(($activeChannels->get($i)->lastMessage()->type == 'file') && ($activeChannels->get($i)->lastMessage()->type == 'image')): ?>
					<span class="media-meta">
						<i class="ico-attachment"></i>
					</span>
				<?php endif; ?>
			<?php endif; ?>
			<span class="media-meta pull-right"><?php echo e(carbonToReadableString($activeChannels->get($i)->otherUser()->last_seen_at)); ?></span>
		</span>
	</a>
<?php endfor; ?>

<hr />
<p class="menu-divider">
	<span>ARCHIEVE</span>
</p>
<hr />
<?php for($i = 0; $i < $expiredChannels->count(); $i++): ?>
	<a href="#" class="media" onclick="channelSelected('<?php echo e($expiredChannels->get($i)->id); ?>')"> 
		<span class="pull-left">
			<?php if(empty($expiredChannels->get($i)->otherUser()->profile_image)): ?>
				<img src="<?php echo e(config('constants.dummy_profile_image')); ?>" class="media-object img-circle" alt="" width="50px" height="50px;">
			<?php else: ?>
				<img src="<?php echo e('/images/'.$expiredChannels->get($i)->otherUser()->profile_image); ?>" class="media-object img-circle" alt="" width="50px" height="50px;">
			<?php endif; ?>
		</span> 
		<span class="media-body">
			<span class="hasnotification hasnotification-default mr5"></span>
			<span class="media-heading contact-name"><?php echo e($expiredChannels->get($i)->otherUser()->name); ?></span> 
			<span class="media-text ellipsis nm">
				<?php if(!empty($expiredChannels->get($i)->lastMessage())): ?>
					<?php echo e(isset($expiredChannels->get($i)->lastMessage()->text) ? $expiredChannels->get($i)->lastMessage()->text : 'Attachment'); ?>

				<?php else: ?>
					No message exchange found
				<?php endif; ?>
			</span>
			<?php if(!empty($expiredChannels->get($i)->lastMessage())): ?>
				<?php if(($expiredChannels->get($i)->lastMessage()->type == 'file') && ($expiredChannels->get($i)->lastMessage()->type == 'image')): ?>
					<span class="media-meta">
						<i class="ico-attachment"></i>
					</span>
				<?php endif; ?>
			<?php endif; ?>
			<span class="media-meta pull-right"><?php echo e(carbonToReadableString($expiredChannels->get($i)->otherUser()->last_seen_at)); ?></span>
		</span>
	</a>
<?php endfor; ?>