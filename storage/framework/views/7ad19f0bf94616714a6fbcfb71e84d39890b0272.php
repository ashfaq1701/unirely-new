<?php $__env->startSection('panel-heading'); ?>
	View Application Assignment
<?php $__env->stopSection(); ?>

<?php $__env->startSection('panel-body'); ?>
	<h2>Application To <?php echo e($application->university->name); ?></h2>
	<p>Selected Plan: <?php echo e($application->package->name); ?></p>
	<p>Payment Done: <?php echo e($application->package->payment); ?> USD</p>
	<p>Payment Date: <?php echo e($application->createdAt); ?></p>
	<h3>Status: </h3>
	<p>
		<?php if($application->assignments->count() > 0): ?>
			Your application is assigned to a mentor and awaiting for his acceptence.
		<?php else: ?>
			Your application is waiting for admin assignment to a specific mentor.
		<?php endif; ?>
	</p>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('chats.partials.appended-panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>