<?php $__env->startSection('panel-heading'); ?>
	View Application
<?php $__env->stopSection(); ?>

<?php $__env->startSection('panel-body'); ?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-11 col-md-offset-1">
				<h2>Application To <?php echo e($application->university->name); ?></h2>
				<p>Name of the applicant: <?php echo e($application->user->name); ?></p>
				<p>Selected Plan: <?php echo e($application->package->name); ?></p>
				<p>Payment Done: <?php echo e($application->package->payment); ?> USD</p>
				<div class="form-group">
					<label class="control-label" for="select-mentor">Select Mentor</label>
					<select id="select-mentor" name="select-mentor" class="form-control">
						<?php foreach($mentors as $mentor): ?>
						<option value="<?php echo e($mentor->id); ?>"><?php echo e($mentor->name); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<a href="#" class="btn btn-default" id="assign-mentor" onclick="createMentorAssignment(<?php echo e($application->id); ?>)">Assign Mentor</a>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('chats.partials.appended-panel', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>