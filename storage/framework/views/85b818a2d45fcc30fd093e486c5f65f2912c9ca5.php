<div class="panel-heading appended-content">
	<h3 class="panel-title">
		<?php echo $__env->yieldContent('panel-heading'); ?>
	</h3>
</div>

<div class="panel-body appended-content appended-content-body">
	<?php echo $__env->yieldContent('panel-body'); ?>
</div>


<?php echo $__env->yieldContent('panel-footer'); ?>