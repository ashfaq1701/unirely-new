<?php $__env->startSection('page-title'); ?>
	Payment Successful
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
	<p><strong>You subscribed for the following packages. It's under review for a while. Site admin will assign you a mentor shortly.</strong></p>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>University Name</th>
				<th>Plan Name</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($details as $detail): ?>
				<tr>
					<td><?php echo e($detail['university_name']); ?></td>
					<td><?php echo e($detail['package_name']); ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.content-without-sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>