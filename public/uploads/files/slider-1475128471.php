<!-- Slider Container -->
<div class="row">
    <div class="col-md-12 columns">
        <!-- Parallax Container -->
        <div class="parallax parallax-background5" data-stellar-background-ratio="0.6">
            <div class="bg-overlay bg-overlay-gdark"></div>
            <div class="white-space space-small"></div>
            <div class="row">
                <div class="col-md-12">
                    <h3 class="fancy-title_new text-center color-white"><span>We Train Top Earning </span>  <strong>Webcam Models.</strong></h3>
                    <!--     <h1 class="perweek text-center color-white"></h1>-->
                </div>
            </div>
            <div class="white-space space-mini"></div>
            <!--2 part-->
            <div class="container">
                <div class="row">
                    <div class="col-md-6 columns">
                        <img alt="videos/home.mp4" datah="384" dataw="683" onClick="visible_video(video.homepage_top);" class="img-responsive thumbnail popupbox-link" src="<?php echo SITEURL; ?>img/demo/content/bannerimages.jpg">
                        <div class="white-space space-small"></div>
                    </div>
                    <div class="col-md-6 columns loginbg">
                        <div class="white-space space-mini"></div>
                        <h3 class="text-center color-white"> <strong>Start making money!</strong></h3>
                        <div class="white-space space-mini"></div>
                        <!-- Form -->
                        <form class="form-horizontal" action="<?php echo SITEURL;?>application/signup" role="form" method="POST" id="signupForm">
                            <div id="signupFormstatus"></div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <input type="name" class="form-control" name="AccountEmail" id="AccountEmail" placeholder="Email Address">
                                </div>
                                <div class="col-sm-6">
                                    <input type="password" class="form-control" id="Password" name="Password" placeholder="Password" autoComplete="off">
                                </div>
                                <?php
                                $accounyTypes = array();
                                $accounyTypes['girl'] = 'Solo Girl';
                                $accounyTypes['guy'] = 'Solo Guy';
                                $accounyTypes['girlguy'] = 'Girl/Guy Couple';
                                $accounyTypes['girlgirl'] = 'Girl/Girl Couple';
                                $accounyTypes['guyguy'] = 'Guy/Guy Couple';
                                $accounyTypes['trans_mtf'] = 'Trans (M to F)';
                                $accounyTypes['trans_ftm'] = 'Trans (F to M)';
                                ?>
                                <div class="col-sm-12">
                                    <select class="form-control ordering signup-select-box" id="AccountType" name="AccountType">
                                        <option value="" selected="selected">Select Account Type</option>
                                        <?php foreach($accounyTypes as $type => $label):?>
                                            <option value="<?php echo $type;?>" ><?php echo $label;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="col-sm-12">
                                    <button type="submit" class="btn makingmoney center-block" name="send">  Create Account <i class="fa fa-check-circle"></i></button>
                                </div>
                            </div>
                        </form>
                        <!-- /Form -->
                        <div class="white-space space-mini"></div>
                    </div>
                </div>
            </div>
            <!--2 part-->
        </div>
        <!-- /Parallax Container -->
    </div>
</div>
<!-- /Slider Container -->
