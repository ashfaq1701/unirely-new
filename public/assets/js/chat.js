var socket;
var channelId = 0;
var loadedMessageCount = 0;
var userId = '';

jQuery(document).ready(function()
{
	$('#typing-indicator').css('display', 'none');
	socket = window.appSocket;
	userId = getUserId();
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    socket.on('textMessage', function (newMessage) {
        receiveTextMessage(newMessage);
    });
    
    socket.on('fileMessage', function(newMessage) {
    	receiveFileMessage(newMessage);
    });
    
    socket.on('onlineStatus', function(statusJson) {
    	updateChannelOnlineStatus(statusJson);
    });
    
    socket.on('typingStatus', function(typingStatus) {
    	updateTypingStatus(typingStatus);
    });

    socket.connect(function () {
    	
    });
    
    $('.chat-input-text__field').focus(function()
    {
    	sendTypingStatus(1);
    });
    
    $('.chat-input-text__field').focusout(function()
    {
    	sendTypingStatus(0);
    });
    
    $('.chat-input-text__field').keyup(function()
    {
    	var text = $(this).val();
    	if(text.length == 1)
    	{
    		sendTypingStatus(1);
    	}
    	
    });
    
    $('.chat-container').click(function()
    {
    	if(channelId != 0)
    	{	
    		readMessages(channelId);
    	}
    });
    
    $('#chat_file_input').change(function()
    {
    	if(channelId != 0)
    	{
    		uploadAttachment();
    	}
    });
    
    $('.chat-input-text__field').keyup(function(e){
        if(e.keyCode == 13)
        {
            sendTextMessage();
        }
    });
    
    $('#send-message').on('click', function()
    {
    	sendTextMessage();
    });
});

function sendTypingStatus(status)
{
	if(channelId != 0)
	{
		socket.send('typingStatus', createTypingObject(status));
	}
}

function updateTypingStatus(typingStatus)
{
	var typingStatusObj = JSON.parse(typingStatus);
	status = typingStatusObj.status;
	user = typingStatusObj.user;
	channel = typingStatusObj.channel;
	if(status == 1)
	{
		if(channelId == channel.id)
		{
			$('#username-typing').text(user.name);
			$('#typing-indicator').css('display', 'block');
		}
	}
	if(status == 0)
	{
		$('#typing-indicator').css('display', 'none');
	}
}

function updateChannelOnlineStatus(statusJson)
{
	var statusObj = JSON.parse(statusJson);
	var channelId = statusObj.channel_id;
	var online = statusObj.online;
	if(online == 0)
	{
		$('#'+channelId+'-online-status').removeClass();
		$('#'+channelId+'-online-status').addClass("hasnotification hasnotification-default mr5");
	}
	else
	{
		$('#'+channelId+'-online-status').removeClass();
		$('#'+channelId+'-online-status').addClass("hasnotification hasnotification-success mr5");
	}
}

function receiveTextMessage(messageJson)
{
	var message = JSON.parse(messageJson);
	if(message.channel_id == channelId)
	{
		displayTextMessage(message);
	}
	else
	{
		var unread = parseInt($('#unread-'+message.channel_id).text());
		if(isNaN(unread))
		{
			$('#unread-'+message.channel_id).text('1');
			$('#unread-'+message.channel_id).addClass('label label-danger');
		}
		else
		{
			$('#unread-'+message.channel_id).text(unread+1);
		}
	}
	$('#last-message-'+message.channel_id).text(message.text);
}

function receiveFileMessage(messageJson)
{
	var message = JSON.parse(messageJson);
	if(message.channel_id == channelId)
	{
		displayFileMessage(message);
	}
	else
	{
		var unread = parseInt($('#unread-'+message.channel_id).text());
		if(isNaN(unread))
		{
			$('#unread-'+message.channel_id).text('1');
		}
		else
		{
			$('#unread-'+message.channel_id).text(unread+1);
		}
	}
	$('#last-message-'+message.channel_id).text('New File');
}

function channelSelected(id)
{
	$('.appended-content').remove();
	$('.chat-container').css('display', 'block');
	if(id != channelId)
	{
		channelId = id;
		loadedMessageCount = 0;
		jQuery('.chat-canvas').empty();
		$('#typing-indicator').css('display', 'none');
		jQuery.ajax(
		{
			url: '/chat/top/'+id,
			type: 'GET',
			success: function(response)
			{
				reinitializeChannel();
				$('.head-to-append').append(response);
			}
		});
	
		loadMessages();
	}
	readMessages(id);
}

function sendTextMessage()
{
	var message = jQuery('.chat-input-text__field').val();
	if((message != '') && (channelId != 0))
	{
		socket.send('textMessage', createTextMessageObject(message));
		jQuery('.chat-input-text__field').val('');
	}
}

function sendFileMessage(filename)
{
	socket.send('fileMessage', createFileMessageObject(filename));
}

function readMessages(channelId)
{
	jQuery.ajax(
	{
		url: '/channel/markread/'+channelId,
		type: 'GET',
		success: function(response)
		{
			$('#unread-'+channelId).text('');
			$('#unread-'+channelId).removeClass('label label-danger');
		}
	})
}

function loadMessages()
{
	jQuery.ajax(
		{
			url: '/channel/messages/'+channelId+'/'+loadedMessageCount,
			type: 'GET',
			success: function(response)
			{
				loadedMessageCount++;
				var messages = response.messages;
				var more = response.more;
				for(var i in messages)
				{
					message = messages[i];
					if(message.type == 'text')
					{
						displayTextMessage(message);
					}
					else
					{
						displayFileMessage(message);
					}
				}
				if(more == true)
				{
					jQuery('.chat-canvas').append('<a id="load-more-link" href="#" onclick="loadOldMessages()">Load More</a><br/>');
				}
				else
				{
					jQuery('#load-more-link').remove();
				}
			}
		}
	);
}

function loadOldMessages()
{
	jQuery.ajax(
		{
			url: '/channel/messages/'+channelId+'/'+loadedMessageCount,
			type: 'GET',
			success: function(response)
			{
				loadedMessageCount++;
				var messages = response.messages;
				var more = response.more;
				for(var i in messages)
				{
					message = messages[i];
					if(message.type == 'text')
					{
						displayOldTextMessage(message);
					}
					else if(message.text == 'file')
					{
						displayOldFileMessage(message);
					}
				}
				if(more == true)
				{
					jQuery('.chat-canvas').append('<a id="load-more-link" href="#" onclick="loadOldMessages()">Load More</a><br/>');
				}
				else
				{
					jQuery('#load-more-link').remove();
				}
			}
		}
	);
}

function createTypingObject(status)
{
	var typingObject = {
		'channel_id': channelId,
		'status': status
	};
	return JSON.stringify(typingObject);
}



function createTextMessageObject(message)
{
	var messageObject = {
		'channel_id': channelId,
		'text': message
	};
	return JSON.stringify(messageObject);
}

function createFileMessageObject(filename)
{
	var messageObject = {
		'channel_id': channelId,
		'file': filename
	};
	return JSON.stringify(messageObject);
}

function getUserId() {
	var name = 'user-id=';
	var ca = document.cookie.split(';');
	for (var i=0 ; i<ca.length ; i++) 
	{
		var c = ca[i];
	    while (c.charAt(0)==' ') c = c.substring(1);
	    if (c.indexOf(name) == 0) {
	      return decodeURIComponent(c.substring(name.length,c.length));
	    }
	}
	return '';
}

function isCurrentUserMessage(message)
{
	if(message.user.email == userId)
	{
		return true;
	}
	return false;
}

function reinitializeChannel()
{
	jQuery('.head-to-append').empty();
	jQuery('.chat-canvas').empty();
}

function getTextMessage(message)
{
	var msg = '';
	var userClass = '';
	var timeClass = '';
	if(isCurrentUserMessage(message))
	{
		userClass = 'media-right';
		timeClass = 'pull-right';
	}
	else
	{
		userClass = 'media';
		timeClass = 'pull-left';
	}
	msg = '<li class="media ' + userClass + '"><a href="javascript:void(0);" class="media-object"> <img src="/images/'
		+ message.user.profile_image +
		'" class="img-circle" alt=""></a>'+
		'<div class="media-body">'+
		'<p class="media-text">'+message.text+'</p>'+
		'<span class="media-meta '+timeClass+'">'+carbonToReadableString(message.created_at)+'</span>'+
		'</div></li>';
	return msg;
}

function getFileMessage(message)
{
	var msg = '';
	var userClass = '';
	var timeClass = '';
	if(isCurrentUserMessage(message))
	{
		userClass = 'media-right';
		timeClass = 'pull-right';
	}
	else
	{
		userClass = 'media';
		timeClass = 'pull-left';
	}
	if((message.type == 'image'))
	{
		msg = '<li class="media ' + userClass + '"><a href="javascript:void(0);" class="media-object"> <img src="/images/'
			+ message.user.profile_image +
			'" class="img-circle" alt=""></a>'+
			'<div class="media-body">'+
			'<p class="media-text">'+'Image : <a href="'+message.file_path+'" target="_blank">' + getFilename(message.file_path) + '<br/>'+
			'<img src="' + message.file_path + '" class="chat-canvas__list-file-img" onload="afterImageLoad(this)">' +
			'</a></p>' +
			'<span class="media-meta '+timeClass+'">'+carbonToReadableString(message.created_at)+'</span>'+
			'</div></li>';
	}
	else
	{
		msg = '<li class="media ' + userClass + '"><a href="javascript:void(0);" class="media-object"> <img src="/images/'
  			+ message.user.profile_image +
  			'" class="img-circle" alt=""></a>'+
  			'<div class="media-body">'+
  			'<p class="media-text">'+'FILE : <a href="'+message.file_path+'" target="_blank">'+message.file_path+'</a></p>'+
  			'<span class="media-meta '+timeClass+'">'+carbonToReadableString(message.created_at)+'</span>'+
  			'</div></li>';
	}
	return msg;
}

function displayOldTextMessage(message)
{
	msg = getTextMessage(message);
	jQuery('.chat-canvas').append(msg);
}

function displayOldFileMessage(message)
{
	msg = getFileMessage(message);
	jQuery('.chat-canvas').append(msg);
}

function displayTextMessage(message)
{
	msg = getTextMessage(message);
	jQuery('.chat-canvas').prepend(msg);
}

function displayFileMessage(message)
{
	msg = getFileMessage(message);
	jQuery('.chat-canvas').prepend(msg);
}

function uploadAttachment()
{
	jQuery('#chat_file_input').fileupload(
	{
		url: '/uploads/file',
		success: function(files)
		{
			sendFileMessage(files[0]);
		},
		error: function(msg)
		{
			console.log(msg);
		}
	});
}

function afterImageLoad(obj) {
	$('.chat-canvas')[0].scrollTop = $('.chat-canvas')[0].scrollTop + obj.height + $('.chat-canvas__list').height();
}

function viewChatsAdmin(localChannelId)
{
	channelId = localChannelId;
	$('#view-chats').modal('show');
	loadMessages();
}