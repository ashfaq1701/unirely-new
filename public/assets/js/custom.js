jQuery(document).ready(function($)
{
	jQuery.expr[':'].contains = function(a, i, m) {
		return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
	};
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	$('.datepicker').datepicker();
	$('.datatable').DataTable();
	$('#view-chats').modal({ show: false});
	
	$('#signup-university').selectize({
	    delimiter: ',',
	    persist: false,
	    valueField: 'id',
	    labelField: 'name',
	    searchField: 'name',
	    maxItems: 1,
	    create: function(input, callback) {
	        $.ajax({
	        	url: '/university/request',
	        	type: 'POST',
	        	dataType: 'json',
	        	data: {
	        		'name': input
	        	},
	        	error: function()
	        	{
	        		callback();
	        	},
	        	success: function(res)
	        	{
	        		callback(res)
	        	}
	        });
	    },
	    render: {
	        option: function(item, escape) {
	            return '<div class="option" data-selectable="" data-value="'+item.id+'">'
	            		+item.name+'</div>';
	        }
	    },
	    load: function(query, callback) {
	    	if (!query.length) return callback();
	    	$.ajax({
	    		url: '/universities/find',
	    		type: 'GET',
	    		dataType: 'json',
	    		data: {
	    			name: query,
	    		},
	    		error: function() {
	    			callback();
	    		},
	    		success: function(res) {
	    			callback(res);
	    		}
	    	});
	    }
	});
	
	var universityArray = {};
	$('select#role').change(function()
	{
		if($('select#role option:selected').text() == 'Mentor')
		{
			$('#university-select-wrapper').removeClass('invisible');
		}
		else
		{
			if($('#university-select-wrapper').hasClass('invisible') == false)
			{
				$('#university-select-wrapper').addClass('invisible');
			}
		}
	});
	
	$('.university-plan').change(function()
	{
		if($(this).is(":checked"))
		{
			var id = $(this).attr('id');
			var idParts = id.split("-");
			var universityId = idParts[idParts.length - 2];
			if($('#university-'+universityId).is(":checked"))
			{
				universityArray['university-'+universityId] = $(this).val();
				totalPrice();
			}
		}
	});
	
	$('.university-input').change(function()
	{
		if($(this).is(":checked"))
		{
			if($(this).parent().find('.price-wrapper').hasClass('invisible'))
			{
				$(this).parent().find('.price-wrapper').removeClass('invisible');
			}
		}
		else
		{
			if(!$(this).parent().find('.price-wrapper').hasClass('invisible'))
			{
				$(this).parent().find('.price-wrapper').addClass('invisible');
			}
			delete universityArray['university-'+$(this).val()];
			totalPrice();
		}
	});
	
	$('#university-modal-submit').click(function() 
	{
		for (var key in universityArray)
		{
			if(!$('#'+key).is(":checked"))
			{
				delete universityArray[key];
			}
		}
		var form = $('#university_submit');
		form.append($('<input type="hidden" name="applications">').val(JSON.stringify(universityArray)));
		form.get(0).submit();
	});
	$('#search-universities').on('input',function(e) {
		 var text = $(this).val();
		 $(".single-university-name:not(:contains('" + text + "'))").parent().parent().css( "display", "none" );
		 $(".single-university-name:contains('" + text + "')").parent().parent().css( "display", "block" );
	});
	$('#request-university').click(function()
	{
		$.ajax(
			{
				method: "POST",
				url: "/university/request",
				data: {name: $('#request-university-name').val()}
			}
		)
		.done(function( msg ) {
			bootbox.alert('University add request submitted for review');
		});
	});
	
	$('#search-contacts').on('input', function(e) {
		var text = $(this).val();
		$(".contact-name:not(:contains('" + text + "'))").parent().parent().css( "display", "none" );
		$(".contact-name:contains('" + text + "')").parent().parent().css( "display", "block" );
	});
	
	function totalPrice()
	{
		var totalPrice = 0;
		for (var key in universityArray)
		{
			totalPrice += Number(universityArray[key]);
		}
		$('#total-price').text(totalPrice.toFixed(2));
	}
	
	
	//Stripe
	
	var $form = $('#payment-form');
	$form.submit(function(event) {
		$form.find('.submit').prop('disabled', true);
		Stripe.card.createToken($form, stripeResponseHandler);
		return false;
	});
	
	$('.package-input').change(function() {
		var id = $(this).attr('data-id');
		$('#package').val(id);
	});
});

function assignMentor(applicationId)
{
	jQuery.ajax(
	{
		method: "GET",
		url: "/application/assign/"+applicationId
	})
	.done(function( msg ) {
		jQuery('.chat-container').css('display', 'none');
		jQuery('.appended-content').remove();
		jQuery('.right-section').append(msg);
	});
}

function searchMentors()
{
	var input = jQuery('#search-user').val();
	$(".mentor-name:not(:contains('" + input + "'))").parent().css( "display", "none" );
	$(".mentor-name:contains('" + input + "')").parent().css( "display", "block" );
}

function createMentorAssignment(applicationId)
{
	var mentorId = $('#select-mentor').val();
	if(mentorId != null)
	{
		jQuery.ajax(
		{
			method: "GET",
			url: "assign/application/"+applicationId+"/"+mentorId
		})
		.done(function(msg) {
			jQuery('#application-container-'+applicationId).remove();
			bootbox.alert(msg.message);
		});
	}
}

function viewApplication(assignmentId)
{
	jQuery.ajax(
	{
		method: "GET",
		url: "/assignment/view/"+assignmentId
	})
	.done(function( msg ) {
		jQuery('.chat-container').css('display', 'none');
		jQuery('.appended-content').remove();
		jQuery('.right-section').append(msg);
	});
}

function viewApplicationStatus(applicationId)
{
	jQuery.ajax(
	{
		method: "GET",
		url: "/application/view/"+applicationId
	})
	.done(function( msg ) {
		jQuery('.chat-container').css('display', 'none');
		jQuery('.appended-content').remove();
		jQuery('.right-section').append(msg);
	});
}

function acceptAssignment(assignmentId)
{
	jQuery.ajax(
	{
		method: "GET",
		url: "/assignment/accept/"+assignmentId
	})
	.done(function( msg ) {
		jQuery('.pending-'+assignmentId).remove();
		jQuery('#assignment-container-'+assignmentId).remove();
		jQuery(msg).insertAfter('#active-head');
		bootbox.alert('You have now a new active channel for accepting the assignment');
	});
}

function rejectAssignment(assignmentId)
{
	jQuery.ajax(
	{
		method: "GET",
		url: "/assignment/reject/"+assignmentId
	})
	.done(function( msg ) {
		jQuery('#assignment-container-'+assignmentId).remove();
		bootbox.alert(msg.message);
	});
}

function approveMentor(mentorId)
{
	bootbox.confirm("Are you sure?", function(result) {	 
		jQuery.ajax(
		{
			method: "GET",
			url: "/mentor/approve/"+mentorId
		})
		.done(function(msg)
		{
			window.location.replace("/mentors/pending");
		});
	});
}

function banMentor(mentorId)
{
	bootbox.confirm("Are you sure?", function(result) {	 
		jQuery.ajax(
		{
			method: "GET",
			url: "/mentor/bann/"+mentorId
		})
		.done(function(msg)
		{
			window.location.replace("/mentors");
		});
	});
}

function approveUniversity(universityId)
{
	bootbox.confirm("Are you sure?", function(result) {	 
		jQuery.ajax(
		{
			method: "GET",
			url: "/university/approve/"+universityId
		})
		.done(function(msg)
		{
			window.location.replace("/universities");
		});
	});
}

function banUniversity(universityId)
{
	bootbox.confirm("Are you sure?", function(result) {	 
		jQuery.ajax(
		{
			method: "GET",
			url: "/university/bann/"+universityId
		})
		.done(function(msg)
		{
			window.location.replace("/universities");
		});
	});
}