jQuery(document).ready(function($)
{
	var $content, $modal, $apnData, $modalCon; 
	$content = $(".min");
	
	$(".modalMinimize").on("click", function() {
		$modalCon = $(this).closest(".minimizableModal").attr("id");
		$apnData = $(this).closest(".minimizableModal");
		$modal = "#" + $modalCon;
		$(".modal-backdrop").addClass("display-none");
		$($modal).toggleClass("min");
		if($($modal).hasClass("min")){
			$(".minmaxCon").append($apnData);
			$(this).find("i").toggleClass( 'fa-minus').toggleClass( 'fa-clone');
		} 
		else { 
			$(".container").append($apnData);
			$(this).find("i").toggleClass( 'fa-clone').toggleClass( 'fa-minus');

		};
	});
	
	$("button[data-dismiss='modal']").click(function(){   
		$(this).closest(".minimizableModal").removeClass("min");
		$(".container").removeClass($apnData);   
		$(this).next('.modalMinimize').find("i").removeClass('fa fa-clone').addClass('fa fa-minus');
	}); 
});