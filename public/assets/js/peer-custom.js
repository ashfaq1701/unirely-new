var peer = null;

var callRunning = false;
var callReceived = false;
var globalCall = null, globalStream = null, globalPeerId = 0;
var n = navigator, is_webkit = false;

var ringerElement = null;

jQuery(document).ready(function($)
{
	peer = new Peer({host: PEER_HOST, port: PEER_PORT, path: PEER_PATH});
	
	peer.on('call', function(call) {
		if(globalCall == null)
		{
			receivedCall(call);
		}
	});
	
	peer.on('open', function(id) {
		$.ajax(
			{
				method: "POST",
				url: "/users/peer/save",
				data: { peer_id: id }
			}
		);
	});
	
	$('#call-receive').click(function()
	{
		if(callRunning == false)
		{
			answerCall(globalCall, globalStream);
		}
	});
	
	$('#call-end').click(function()
	{
		if(callRunning == true)
		{
			endCall(globalCall, globalStream);
		}
		else if(callReceived == true)
		{
			rejectCall(globalCall, globalStream);
		}
	});
	
	jQuery('#callModal').on('hidden.bs.modal', function () {
		endCall(globalCall, globalStream);
		globalCall = null;
	});
	
	ringerAudio = new Audio('/assets/sounds/ringer.mp3'); 
	ringerAudio.addEventListener('ended', function() {
	    this.currentTime = 0;
	    this.play();
	}, false);
});

function updateCallUI(peerId, type)
{
	jQuery.ajax(
		{
			url: "/user/peer",
			data: {
				'peer_id': peerId
			},
			success: function(response)
			{
				var user = response.user;
				jQuery('#caller-name').text(user.name);
				jQuery('#call-modal-profile-image').attr('src', '/images/'+user.profile_image);
			}
		}
	)
	if(type == 'received')
	{
		jQuery('#calling-title').text('Call');
		jQuery('#call-appended').text('from');
	}
	else if(type == 'made')
	{
		jQuery('#calling-title').text('Calling');
		jQuery('#call-appended').text('to');
	}
	
	$('#myModal').on('hidden.bs.modal', function () {
		  endCall(globalCall, globalStream);
	});
}

function receivedCallSuccess(stream) {
	  globalStream = stream;
	  var localCall = globalCall;
	  jQuery('#local-vid-container').attr('src', window.URL.createObjectURL(stream));
	  localCall.on('stream', function(remoteStream) {
		  jQuery('#remote-vid-container').attr('src', window.URL.createObjectURL(remoteStream));
		  jQuery('#remote-audio').attr('src', window.URL.createObjectURL(remoteStream));
	  });
	  localCall.on('close', function()
	  {
		  var tracks = stream.getTracks();
		  for(var i = 0; i < tracks.length; i++)
		  {
			  var track = tracks[i];
			  track.stop();
		  }
		  jQuery('#remote-vid-container').attr('src', "");
		  jQuery('#remote-audio').attr('src', "");
		  updateCallUI(globalPeerId, 'received');
		  callRunning = false;
		  globalCall = null;
		  globalStream = null;
	  });
}

function doCallSuccess(stream) {
	  jQuery('#local-vid-container').attr('src', window.URL.createObjectURL(stream));
	  var call = peer.call(globalPeerId, stream);
	  globalCall = call;
	  globalStream = stream;
	  call.on('stream', function(remoteStream) {
		  jQuery('#remote-vid-placeholder').css('display', 'none');
		  jQuery('#remote-vid-container').css('display', 'block');
		  jQuery('#remote-vid-container').attr('src', window.URL.createObjectURL(remoteStream));
		  callRunning = true;
	  });
	  call.on('close', function()
	  {
		  stopCallAlert();
		  if(stream != null)
		  {
			  var tracks = stream.getTracks();
			  for(var i = 0; i < tracks.length; i++)
			  {
				  var track = tracks[i];
				  track.stop();
			  }
			  callRunning = false;
			  globalCall = null;
			  globalStream = null;
		  }
	  });
}

function onError(err) {
	  console.log('Failed to get local stream' ,err);
}

function receivedCall(call)
{
	callReceived = true;
	globalPeerId = call.peer;
	updateCallUI(globalPeerId, 'received');
	globalCall = call;
	refreshCallModal();
	jQuery('#callModal').modal({backdrop: false, keyboard: false});
	ringCallAlert();
	if (n.getUserMedia) {
	    n.getUserMedia({video: true, audio: true}, receivedCallSuccess, onError);
	}
	else if (n.webkitGetUserMedia) {
	    is_webkit = true;
	    n.webkitGetUserMedia({video: true, audio: true}, receivedCallSuccess, onError);
	}
	else if(n.mozGetUserMedia)
	{
		n.mozGetUserMedia({video: true, audio: true}, receivedCallSuccess, onError);
	}
	else {
	    console.log("Your browser doesn't support webkit communication");
	}
}

function doCall(peerId)
{
	if(peerId == 0)
	{
		return 0;
	}
	globalPeerId = peerId;
	callRunning = true;
	updateCallUI(peerId, 'made');
	refreshCallModal();
	jQuery('#callModal').modal({backdrop: false, keyboard: false});
	var n = navigator, is_webkit = false;
	if (n.getUserMedia) {
	    n.getUserMedia({video: true, audio: true}, doCallSuccess, onError);
	}
	else if (n.webkitGetUserMedia) {
	    is_webkit = true;
	    n.webkitGetUserMedia({video: true, audio: true}, doCallSuccess, onError);
	}
	else if(n.mozGetUserMedia)
	{
		n.mozGetUserMedia({video: true, audio: true}, doCallSuccess, onError);
	}
	else {
	    console.log("Your browser doesn't support webkit communication");
	}
}

function answerCall(call, stream)
{
	call.answer(stream);
	stopCallAlert();
	jQuery('#remote-vid-placeholder').css('display', 'none');
	jQuery('#remote-vid-container').css('display', 'block');
	callReceived = false;
	callRunning = true;
}

function endCall(call, stream)
{
	if(call != null)
	{
		console.log(call);
		call.close();
	}
	stopCallAlert();
	if(stream != null)
	{
		var tracks = stream.getTracks();
		for(var i = 0; i < tracks.length; i++)
		{
			var track = tracks[i];
			track.stop();
		}
	}
	jQuery('#remote-vid-container').attr('src', "");
	jQuery('#remote-audio').attr('src', "");
	callReceived = false;
	callRunning = false;
	globalCall = null;
	globalStream = null;
}

function rejectCall(call, stream)
{
	if(call != null)
	{
		call.close();
	}
	if(stream != null)
	{
		var tracks = stream.getTracks();
		for(var i = 0; i < tracks.length; i++)
		{
			var track = tracks[i];
			track.stop();
		}
	}
	stopCallAlert();
	callReceived = false;
}

function ringCallAlert()
{
	ringerAudio.play();
}

function stopCallAlert()
{
	ringerAudio.pause();
}

function refreshCallModal()
{
	jQuery('#remote-vid-placeholder').css('display', 'block');
	jQuery('#remote-vid-container').css('display', 'none');
}
