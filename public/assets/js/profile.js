jQuery(document).ready(function($)
{
	var courseIds = [];
	for(var i = 0; i < courses.length; i++)
	{
		courseIds.push(courses[i].id);
	}
	$('#courses-interested').selectize({
	    delimiter: ',',
	    persist: false,
	    valueField: 'id',
	    labelField: 'name',
	    searchField: 'name',
	    options: courses,
	    items: courseIds,
	    create: function(input, callback) {
	        $.ajax({
	        	url: '/courses/create',
	        	type: 'POST',
	        	dataType: 'json',
	        	data: {
	        		'name': input
	        	},
	        	error: function()
	        	{
	        		callback();
	        	},
	        	success: function(res)
	        	{
	        		callback(res)
	        	}
	        });
	    },
	    render: {
	        option: function(item, escape) {
	            return '<div class="option" data-selectable="" data-value="'+item.id+'">'
	            		+item.name+'</div>';
	        }
	    },
	    load: function(query, callback) {
	    	if (!query.length) return callback();
	    	$.ajax({
	    		url: '/courses/find',
	    		type: 'GET',
	    		dataType: 'json',
	    		data: {
	    			name: query,
	    		},
	    		error: function() {
	    			callback();
	    		},
	    		success: function(res) {
	    			callback(res);
	    		}
	    	});
	    }
	});
	
	$('#profile-form').submit( function() {
		var input = $('#profile-picture');
		var value = input.val();
		var form = $(this);
		if(!(value == null || value == ""))
		{
			input.fileupload(
				{
					url: '/uploads/image',
					success: function(files)
					{
						$.ajax(
							{
								url: '/profile/image',
								type: 'POST',
								data: {
									image: files[0],
									userId: userId 
								},
								async: false
							}
						);
					},
					error: function(msg)
					{
						console.log(msg);
					}
				}
			);
		}
        return true;
    });
});