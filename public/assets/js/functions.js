function stripeResponseHandler(status, response) {
	// Grab the form:
	var $form = $('#payment-form');
	if (response.error) 
	{
		$form.find('.payment-errors').text(response.error.message);
		$form.find('.submit').prop('disabled', false); // Re-enable submission
	} 
	else 
	{
		var token = response.id;
		var cardBrand = response.card.brand;
		var cardLast4 = response.card.last4;
		$form.append($('<input type="hidden" name="cardBrand">').val(cardBrand));
		$form.append($('<input type="hidden" name="stripeToken">').val(token));
		$form.append($('<input type="hidden" name="last4">').val(cardLast4));
		$form.get(0).submit();
	}
}

function carbonToReadableString(dateStr) 
{
	specificDateMs = new Date(dateStr).getTime() - (-0.5)*60*60*1000;
	specificDate = Math.floor(specificDateMs/1000);
	var offset = new Date().getTimezoneOffset();
	nowMs = Date.now() + offset*60*1000 ;
	now = Math.floor(nowMs / 1000);
	if(!dateStr)
	{
		return 'Never';
	}
	else
	{
		diff = now - specificDate;
	
		if (diff < 60){
			if(diff > 1)
			{
				return diff + ' seconds ago';
			}
			else
			{
				return 'a second ago';
			}
		}
	
		diff = Math.floor(diff/60);
	
		if (diff < 60){
			if(diff > 1)
			{
				return diff + ' minutes ago';
			}
			else
			{
				return 'one minute ago';
			}
		}
	
		diff = Math.floor(diff/60);
	
		if (diff < 24){
			if(diff > 1)
			{
				return diff + ' hours ago';
			}
			else
			{
				return 'an hour ago';
			}
		}
	
		diff = Math.floor(diff/24);
	
		if (diff < 7){
			if(diff > 1)
			{
				return diff + ' days ago';
			}
			else
			{
				return 'yesterday';
			}
		}
	
		if (diff < 30)
		{
			diff = Math.floor(diff / 7);
			
			if(diff > 1)
			{
				return diff + ' weeks ago';
			}
			else
			{
				return 'one week ago';
			}
		}
	
		diff = Math.floor(diff/30);
	
		if (diff < 12){
			if(diff > 1)
			{
				return diff + ' months ago';
			}
			else
			{
				return 'last month';
			}
		}
		
		var specificDateObj = new Date(specificDateMs);
		var nowObj = new Date(newMs);
		
		diff = parseInt(newObj.getFullYear()) - parseInt(specificDateObj.getFullYear());
	
		if(diff > 1)
		{
			return diff + ' years ago';
		}
		else
		{
			return 'last year';
		}
	}
}