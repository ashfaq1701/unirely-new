<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UniversitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$file = fopen(__DIR__."/universities.csv","r");
    	while(! feof($file))
    	{
    		$row = fgetcsv($file);
    		$university = $row[0];
    		if(!empty($university))
    		{
    			DB::table('universities')->insert([
    					'name' => $university,
    					'created_at' => Carbon::now(),
    					'approved' => '1'
    			]);
    		}
    		
    	}
    }
}
