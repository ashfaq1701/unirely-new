<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CoursesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('courses')->insert([
				'name' => 'B.Sc in Computer Science And Engineering',
				'created_at' => Carbon::now()
		]);
		DB::table('courses')->insert([
				'name' => 'B.Sc in Electrical and Electronic Engineering',
				'created_at' => Carbon::now()
		]);
		DB::table('courses')->insert([
				'name' => 'B.Sc in Electronics and Communication Engineering',
				'created_at' => Carbon::now()
		]);
		DB::table('courses')->insert([
				'name' => 'B.Sc in Psycology',
				'created_at' => Carbon::now()
		]);
		DB::table('courses')->insert([
				'name' => 'B.Sc in Agriculture',
				'created_at' => Carbon::now()
		]);
	}
}
