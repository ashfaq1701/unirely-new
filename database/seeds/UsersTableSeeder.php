<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$roleId = Role::where('name', 'admin')->first()->id;
    	DB::table('users')->insert([
    		'name' => 'Md Ashfaq Salehin',
    		'email' => 'ashfaq1701@gmail.com',
    		'password' => bcrypt('090927'),
    		'created_at' => Carbon::now(),
    		'profile_image' => config('constants.dummy_profile_image'),
    		'role_id' => $roleId
    	]);
    }
}
