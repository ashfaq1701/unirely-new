<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('roles')->insert([
    		'name' => 'admin',
    		'created_at' => Carbon::now()
    	]);
    	DB::table('roles')->insert([
    		'name' => 'student',
    		'created_at' => Carbon::now()
    	]);
    	DB::table('roles')->insert([
    		'name' => 'mentor',
    		'created_at' => Carbon::now()
    	]);
    }
}
