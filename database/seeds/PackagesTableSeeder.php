<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('packages')->insert([
    			'name' => 'Basic',
    			'validity' => '5',
    			'payment' => '50',
    			'created_at' => Carbon::now()
    	]);
    	DB::table('packages')->insert([
    			'name' => 'Premium',
    			'validity' => '14',
    			'payment' => '150',
    			'created_at' => Carbon::now()
    	]);
    	DB::table('packages')->insert([
    			'name' => 'Premium Plus',
    			'validity' => '21',
    			'payment' => '250',
    			'created_at' => Carbon::now()
    	]);
    }
}
