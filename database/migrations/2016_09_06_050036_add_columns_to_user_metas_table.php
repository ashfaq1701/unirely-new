<?php

use Illuminate\Database\Migrations\Migration;

class AddColumnsToUserMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::table('user_metas', function ($table) {
    		$table->string('city', 100)->nullable();
    		$table->string('percentage', 10)->nullable();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	Schema::table('user_metas', function ($table) {
    		$table->dropColumn('city');
    		$table->dropColumn('percentage');
    	});
    }
}
