<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWithdrawlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	Schema::create('withdrawls', function (Blueprint $table) {
    		$table->increments('id');
    		$table->decimal('amount', 10, 2);
    		$table->integer('user_id');
    		$table->string('status', 10)->default('pending')->nullable();
    		$table->timestamps();
    	});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('withdrawls');
    }
}
